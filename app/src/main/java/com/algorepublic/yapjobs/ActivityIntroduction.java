package com.algorepublic.yapjobs;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.algorepublic.yapjobs.Adapters.CustomPagerAdapter;
import com.algorepublic.yapjobs.Adapters.IntroItems;
import com.algorepublic.yapjobs.Fragments.ActivityProfileSteps;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.androidquery.AQuery;
import com.viewpagerindicator.CirclePageIndicator;
import com.yqritc.scalablevideoview.ScalableVideoView;

import java.io.IOException;
import java.util.ArrayList;

public class ActivityIntroduction extends AppCompatActivity {

    AQuery aq;
    ScalableVideoView videoView;
    CirclePageIndicator circlePageIndicator;
    ViewPager pager;
    ArrayList<IntroItems> arrayList;
    CustomPagerAdapter adapter;
    MyTimer timer;
    int position;
    BaseClass baseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        baseClass = ((BaseClass) getApplicationContext());
        if(baseClass.getIsUserExsit().equalsIgnoreCase("1"))
        {
            startActivity(new Intent(this, BaseActivity.class));
            finish();
        }
        aq = new AQuery(this);
        videoView = (ScalableVideoView) findViewById(R.id.surface);
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.circularIndicator);
        pager = (ViewPager)findViewById(R.id.pager);
        arrayList = GetSearchResults();
        adapter = new CustomPagerAdapter(this,arrayList);
        pager.setAdapter(adapter);
        circlePageIndicator.setViewPager(pager);
        timer = new MyTimer(3000,3000);
        timer.start();
        position=0;
        try {
            videoView.setRawData(R.raw.introduction);
            videoView.setVolume(0, 0);
            videoView.setLooping(true);
            videoView.prepare(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
                }
            });
        } catch (IOException ioe) {
            //ignore
        }

        aq.id(R.id.statusbar).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityIntroduction.this,ActivityProfileSteps.class));
                finish();
            }
        });

    }

    static int inner;
    private static ArrayList<IntroItems> GetSearchResults() {
        ArrayList<IntroItems> results = new ArrayList<IntroItems>();




        for (inner=1;inner<4;inner++) {
            IntroItems item_details = new IntroItems();
            if (inner == 1) {
                item_details.setTitle("EASY STUFF");
                item_details.setDesc("YapJobs make finding and applying for jobs simple, easy and fun");
                results.add(item_details);
            }
            if (inner == 2){
                item_details.setTitle("GET NOTIFIED" );
                item_details.setDesc("Get real time updates as soon as job is posted. No stale job adds");
                results.add(item_details);
            }
            if (inner == 3){
                item_details.setTitle("GET INVITED" );
                item_details.setDesc("Let employers discover you and invite you to the job");
                results.add(item_details);
            }
        }

        return results;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_introduction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyTimer extends CountDownTimer
    {

        public MyTimer(long startTime, long interval)
        {
            super(startTime, interval);
        }
        @Override
        public void onFinish()
        {
            if(position==3)
            {
                position=0;
            }
            pager.setCurrentItem(position);
            pager.setAnimationCacheEnabled(true);
            position++;
            timer.start();
        }
        @Override
        public void onTick(long millisUntilFinished)
        {
        }
    }
}
