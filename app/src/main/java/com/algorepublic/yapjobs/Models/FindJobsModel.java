package com.algorepublic.yapjobs.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by android on 8/17/15.
 */
public class FindJobsModel {

    private static FindJobsModel _obj=null;

    public FindJobsModel(){

    }

    public static FindJobsModel getInstance(){
        if (_obj==null){
            _obj = new FindJobsModel();
        }
        return _obj;
    }
    public void setList(FindJobsModel obj) {
        _obj = obj;
    }

    @SerializedName("array")
    public ArrayList<Jobs> jobs = new ArrayList<Jobs>();

    public class Jobs {

        @SerializedName("id")
        public String id;

        @SerializedName("start_date_format")
        public String start_date_format;

        @SerializedName("distance")
        public String distance;

        @SerializedName("hourly_rate")
        public String hourly_rate;

        @SerializedName("role")
        Role role =new Role();

        @SerializedName("business")
        Business business =new Business();

    }

    public class Role {

        @SerializedName("name")
        public String name;

        @SerializedName("image")
        public String image;

    }

    public class Business {

        @SerializedName("name")
        public String name;

    }


}
