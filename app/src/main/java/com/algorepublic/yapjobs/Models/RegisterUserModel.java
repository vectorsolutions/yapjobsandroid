package com.algorepublic.yapjobs.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 7/14/15.
 */
public class RegisterUserModel {

    private static RegisterUserModel _obj=null;

    public RegisterUserModel(){

    }

    public static RegisterUserModel getInstance(){
        if (_obj==null){
            _obj = new RegisterUserModel();
        }
        return _obj;
    }
    public void setList(RegisterUserModel obj) {
        _obj = obj;
    }

    @SerializedName("status")
    public int status;

    @SerializedName("message")
    public String message;


}
