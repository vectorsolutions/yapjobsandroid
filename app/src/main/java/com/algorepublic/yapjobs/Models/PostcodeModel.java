package com.algorepublic.yapjobs.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 8/27/15.
 */
public class PostcodeModel {

    private static PostcodeModel _obj = null ;

    public PostcodeModel(){

    }
    public static PostcodeModel getInstance(){
        if (_obj==null){
            _obj = new PostcodeModel();
        }
        return _obj;
    }

    public void setList(PostcodeModel obj) {
        _obj = obj;
    }

    @SerializedName("status")
    public int status;

    @SerializedName("result")
    public String result;

    @SerializedName("error")
    public String error;

}
