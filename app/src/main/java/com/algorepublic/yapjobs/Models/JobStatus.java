package com.algorepublic.yapjobs.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by android on 8/27/15.
 */
public class JobStatus {
    public static JobStatus _obj = null;
    public JobStatus(){

    }

    public static JobStatus getInstance(){
        if (_obj==null){
            _obj = new JobStatus();
        }
        return _obj;
    }


    public void setList(JobStatus obj){
        _obj=obj;
    }


    @SerializedName("status")
    public int status;

    @SerializedName("message")
    public String message;

    @SerializedName("apply_quota_remaining")
    public int apply_quota_remaining;
}
