package com.algorepublic.yapjobs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.algorepublic.yapjobs.Fragments.FragmentAboutMe;
import com.algorepublic.yapjobs.Fragments.FragmentJobs;
import com.algorepublic.yapjobs.Fragments.FragmentMyExperience;
import com.algorepublic.yapjobs.Fragments.FragmentRoles;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import io.techery.progresshint.ProgressHintDelegate;

public class BaseActivity extends AppCompatActivity {


    private MenuDrawer mDrawerLeft;
    private RelativeLayout btn_menu;
    private Toolbar mToolbar;

    AQuery aq,aq_radius;
    AlertDialog alertRadius;
    View searchRadius;
    BaseClass baseClass;
    ImageView img_logo;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    private Intent pictureActionIntent = null;
    Bitmap bitmap;

    String selectedImagePath;
    boolean flag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aq = new AQuery(this);

        baseClass= ((BaseClass) getApplicationContext());
        mDrawerLeft = MenuDrawer.attach(this, MenuDrawer.Type.OVERLAY, Position.START, MenuDrawer.MENU_DRAG_CONTENT);
        mDrawerLeft.setContentView(R.layout.activity_base);
        mDrawerLeft.setMenuView(R.layout.layout_dropdownmenu);
        mDrawerLeft.setDrawOverlay(true);
        mDrawerLeft.setDrawerIndicatorEnabled(true);
        mDrawerLeft.setupUpIndicator(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btn_menu = (RelativeLayout) mToolbar.findViewById(R.id.menu);
        img_logo = (ImageView) findViewById(R.id.user_image);
        aq.id(R.id.internal).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Regular));
        aq.id(R.id.external).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Regular));
        aq.id(R.id.category).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Regular));
        aq.id(R.id.Explorar).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Regular));
        aq.id(R.id.displayname).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Regular));
        if(savedInstanceState == null) {
            callFragment(R.id.base_container, FragmentJobs.newInstance(), "Jobs");
        }
        aq.id(R.id.user_image).image(Base64ToBitmap(baseClass.getImageBitmap()));
        aq.id(R.id.user_image).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(intent, 0);
                showDialog();

            }
        });
        Log.e("name", baseClass.getFirstName() + " " + baseClass.getLastName());
        aq.id(R.id.displayname).text(baseClass.getFirstName() + " " + baseClass.getLastName());
        if(!baseClass.getImageBitmap().isEmpty())
            aq.id(R.id.user_image).image(Base64ToBitmap(baseClass.getImageBitmap()));
        else
            aq.id(R.id.user_image).image(baseClass.getImageUrl());
        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.toggleMenu();
            }
        });
        LayoutInflater inflater = getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);

        aq.id(R.id.layout_myrole).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.closeMenu();
                callFragmentMenu(R.id.base_container, FragmentRoles.newInstance(), "Step2");
            }
        });
        aq.id(R.id.layout_aboutme).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.closeMenu();
                callFragmentMenu(R.id.base_container, FragmentAboutMe
                        .newInstance(), "AboutMe");
            }
        });
        ////////////////////Search Radius/////////////////////////////////////////////////
        searchRadius = inflater.inflate(R.layout.fragment_searchradius,
                null, false);
        aq_radius = new AQuery(searchRadius);
        builder.setView(searchRadius);
        alertRadius = builder.create();
        alertRadius.setCanceledOnTouchOutside(false);
        aq_radius.id(R.id.search_radius).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Black));
        aq_radius.id(R.id.text).getTextView().setTypeface(TypeFaces.get(BaseActivity.this, BaseClass.SourceSansPro_Black));
        aq.id(R.id.layout_search_radius).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.closeMenu();
                alertRadius.show();
            }
        });
        aq_radius.id(R.id.close_dialog).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertRadius.dismiss();
            }
        });
        ((io.techery.progresshint.addition.widget.SeekBar) searchRadius.findViewById(R.id.seekbar_radius)).getHintDelegate()
                .setHintAdapter(new ProgressHintDelegate.SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        baseClass.setRadius(String.valueOf(progress));
                        return String.valueOf(progress);
                    }
                });

        aq_radius.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncUpdateUser().execute();
            }
        });

        aq.id(R.id.layout_myexperience).getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLeft.closeMenu();
                callFragmentMenu(R.id.base_container, FragmentMyExperience.newInstance(), "Step3");
            }
        });
    }


    protected void callFragment(int containerId, Fragment fragment, String tag){
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerId,fragment,tag)
                .commit();
    }

    protected void callFragmentMenu(int containerId, Fragment fragment, String tag){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    Bitmap Base64ToBitmap(String myImageData)
    {
        byte[] imageAsBytes = Base64.decode(myImageData.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    public class AsyncUpdateUser extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Update_User_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            Crouton.makeText(BaseActivity.this, "Changes Saved", Style.CONFIRM).show();
            alertRadius.dismiss();
//            getActivity().getSupportFragmentManager().popBackStack();
//            PopulateModel(result);
        }
    }

    public String  GetJsonObject() throws JSONException {

        Log.e("String", baseClass.getPhoneNumber() + "/" + baseClass.getExpROle() + "/" + baseClass.getExpTime() + "/" + baseClass.getTotalRoles()
                + "/" + baseClass.getEduLevel() + "/" + baseClass.getEligible() + "/" + baseClass.getRadius() + "/" + baseClass.getLat() + "," + baseClass.getLon());

        JSONObject object = new JSONObject();
        JSONObject userObj = new JSONObject();

        object.put("postcode", "SE1 1TL");
        object.put("city", "Karachi");
        object.put("location", baseClass.getLat()+","+baseClass.getLon());
        object.put("date_of_birth", "1983-06-12");
        object.put("first_name", baseClass.getFirstName());
        object.put("last_name", baseClass.getLastName());
        object.put("profile_image", "http,//www.matchninja.com/wp-content/uploads/2014/08/online-dating-profile.jpg");
        object.put("about_me", "I'm 23");
        object.put("experiences", baseClass.getExpTime());
        object.put("allowed_working_in_uk", baseClass.getEligible());
        object.put("search_job_radius", baseClass.getRadius());
        object.put("is_available", true);
        object.put("is_active", false);
        object.put("education_level", baseClass.getEduLevel());

        userObj.put("job_seeker", object);
        userObj.put("interested_in", baseClass.getExpROle());
        userObj.put("experiences", baseClass.getTotalRoles());

        return userObj.toString();
    }

    private void startDialog() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
        myAlertDialog.setTitle("Upload Pictures Option");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pictureActionIntent = new Intent(
                                Intent.ACTION_GET_CONTENT, null);
                        pictureActionIntent.setType("image/*");
                        pictureActionIntent.putExtra("return-data", true);
                        startActivityForResult(pictureActionIntent,
                                GALLERY_PICTURE);
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pictureActionIntent = new Intent(
                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(pictureActionIntent,
                                CAMERA_REQUEST);

                    }
                });
        myAlertDialog.show();
    }

    public void showDialog(){
        // custom dialog
        final Dialog dialog = new Dialog(BaseActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_popup);
        dialog.show();
//        baseClass.setLocalPopup(true);
        TextView text = (TextView) dialog.findViewById(R.id.textView1);
        TextView textView = (TextView) dialog.findViewById(R.id.textView);
        Button remove = (Button) dialog.findViewById(R.id.remove);
//        textView.setTypeface(TypeFaces.get(getActivity(), BaseClass.BenchNine_TYPEFACE));
//        text.setText(R.string.popup_text);
        Log.e("Tag",baseClass.getImageTag()+"");
        if (baseClass.getImageTag())
            remove.setVisibility(View.VISIBLE);
        Button camera = (Button) dialog.findViewById(R.id.camera);
        Button gallery = (Button) dialog.findViewById(R.id.gallery);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureActionIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(pictureActionIntent,
                        CAMERA_REQUEST);
                dialog.dismiss();
//                 Crouton.makeText(getActivity(), "Book is saved locally", Style.ALERT).show();

            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureActionIntent = new Intent(
                        Intent.ACTION_GET_CONTENT, null);
                pictureActionIntent.setType("image/*");
                pictureActionIntent.putExtra("return-data", true);
                startActivityForResult(pictureActionIntent,
                        GALLERY_PICTURE);
                dialog.dismiss();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseClass.setImageTag(false);
                img_logo.setImageBitmap(null);
                dialog.dismiss();
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    // our BitmapDrawable for the thumbnail
                    BitmapDrawable bmpDrawable = null;
                    // try to retrieve the image using the data from the intent
                    Cursor cursor = getContentResolver().query(data.getData(),
                            null, null, null, null);
                    if (cursor != null) {

                        cursor.moveToFirst();

                        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                        String fileSrc = cursor.getString(idx);
                        bitmap = BitmapFactory.decodeFile(fileSrc); // load
                        // preview
                        // image
                        bitmap = Bitmap.createScaledBitmap(bitmap,
                                100, 100, false);
                        // bmpDrawable = new BitmapDrawable(bitmapPreview);
                        img_logo.setImageBitmap(bitmap);

                        baseClass.setImageTag(true);

                    } else {

                        bmpDrawable = new BitmapDrawable(getResources(), data
                                .getData().getPath());
                        img_logo.setImageDrawable(bmpDrawable);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra("data")) {

                    // retrieve the bitmap from the intent
                    bitmap = (Bitmap) data.getExtras().get("data");


                    Cursor cursor = getContentResolver()
                            .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    new String[] {
                                            MediaStore.Images.Media.DATA,
                                            MediaStore.Images.Media.DATE_ADDED,
                                            MediaStore.Images.ImageColumns.ORIENTATION },
                                    MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            Uri uri = Uri.parse(cursor.getString(cursor
                                    .getColumnIndex(MediaStore.Images.Media.DATA)));
                            selectedImagePath = uri.toString();
                        } while (cursor.moveToNext());
                        cursor.close();
                    }

                    Log.e("path of the image ", selectedImagePath);


                    bitmap = Bitmap.createScaledBitmap(bitmap, 100,
                            100, false);
                    // update the image view with the bitmap
                    img_logo.setImageBitmap(bitmap);
                    baseClass.setImageTag(true);
                } else if (data.getExtras() == null) {

                    Toast.makeText(getApplicationContext(),
                            "No extras to retrieve!", Toast.LENGTH_SHORT)
                            .show();

                    BitmapDrawable thumbnail = new BitmapDrawable(
                            getResources(), data.getData().getPath());

                    // update the image view with the newly created drawable
                    img_logo.setImageDrawable(thumbnail);


                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

}
