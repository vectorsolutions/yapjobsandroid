package com.algorepublic.yapjobs.DB;


public class RoleList {
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleSlug() {
		return roleSlug;
	}
	public void setRoleSlug(String roleSlug) {
		this.roleSlug = roleSlug;
	}
    public void setRolePosition(String rolePosition) {
        this.rolePosition = rolePosition;
    }
	public String getRolePosition() {
		return rolePosition;
	}
	
	private String rolePosition ;
	private String roleName;
	private String roleSlug;
}
