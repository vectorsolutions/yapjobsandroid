package com.algorepublic.yapjobs.Utils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 8/17/15.
 */
public class GenericHttpClient {

    private Header[] generateHttpRequestHeaders() {

        List<Header> result = new ArrayList<Header>();
        result.add(new BasicHeader("Content-type", "application/json"));
        result.add(new BasicHeader("Authorization", "68b39e43f504af0ca08c884d54d1a32d"));
        return result.toArray(new Header[]{});
    }

    public String post(String url,String obj) throws IOException {

        HttpClient hc = new DefaultHttpClient();
        String message =null;
        HttpPost p = new HttpPost(url);

        try {
            p.setEntity(new StringEntity(obj, "UTF8"));
            p.setHeaders(generateHttpRequestHeaders());
            HttpResponse resp = hc.execute(p);
            if (resp != null) {
                message = convertStreamToString(resp.getEntity().getContent());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public String get(String url) throws IOException {

        HttpClient hc = new DefaultHttpClient();
        String message =null;
        HttpGet p = new HttpGet(url);

        try {
            p.setHeaders(generateHttpRequestHeaders());
            HttpResponse resp = hc.execute(p);
            if (resp != null) {
                message = convertStreamToString(resp.getEntity().getContent());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }
    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
