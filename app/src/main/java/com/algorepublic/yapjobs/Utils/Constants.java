package com.algorepublic.yapjobs.Utils;

/**
 * Created by hasanali on 12/06/2015.
 */
public class Constants {
    public static int STATUS_OK = 200, INTERNET_FAILURE = 101;
    public static String GENERAL_ERROR = "There was some error in server please try again later.";
    public static String BASE_DEV = "http://yapjobs-qa.herokuapp.com/";
    public static String BASE_URL = "https://yapjobs-qa.herokuapp.com/api/v3/";
    public static String Register_URL = "users";
    public static String Jobs_URL = "job_seekers/jobs?";
    public static String Apply_URL = "jobs/apply?";
    public static String Decline_URL = "jobs/decline?";
    public static String Profile_URL = "job_seekers/profile?";
    public static String Update_User_URL = "job_seekers/update_profile?";
    public static String JobsRoles_URL = "job_roles";
    public static String Find_Jobs_URL = "job_seekers/jobs?";
}
