package com.algorepublic.yapjobs.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;

import com.parse.Parse;


/**
 * Created by hasanali on 27/08/14.
 */
public class BaseClass extends Application {

    public static String LINKEDIN_CONSUMER_KEY = "77rggcqw6khtt9";
    public static String LINKEDIN_CONSUMER_SECRET = "6tCWW2tK9GhgSShr";

    public static String scopeParams = "rw_nus+r_basicprofile";

    public static String OAUTH_CALLBACK_SCHEME = "x-oauthflow-linkedin";
    public static String OAUTH_CALLBACK_HOST = "callback";
    public static String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;
    /**
     * Init preferences
     */
    public static String SourceSansPro_Regular = "SourceSansPro-Regular.tff";
    public static String SourceSansPro_Semibold = "SourceSansPro-Semibold.tff";
    public static String SourceSansPro_Light = "SourceSansPro-Light.tff";
    public static String SourceSansPro_Blod = "SourceSansPro-Bold.tff";
    public static String SourceSansPro_Black = "SourceSansPro-Black.tff";
    public static String Quicksand_Bold = "Quicksand-Bold.ttf";
    public static String Quicksand_Regular = "Quicksand-Regular.ttf";
    public SharedPreferences appSharedPrefs;
    public SharedPreferences.Editor prefsEditor;
    public String SHARED_NAME = "com.algorepublic.yapjobs";
    //Shared preferences tags
    private String FirstName = "FirstName";
    private String LastName = "LastName";
    private String PostCode = "PostCode";
    private String ImageUrl = "ImageUrl";
    private String EduLevel = "EduLevel";
    private String Eligible = "Eligible";
    private String ExpROle = "ExpROle";
    private String ExpTime = "ExpTime";
    private String ExpTime1 = "ExpTime1";
    private String ExpTime2 = "ExpTime2";
    private String ExpTime3 = "ExpTime3";
    private String Radius = "Radius";
    private String TotalRoles = "TotalRoles";
    private String PhoneNumber = "Number";
    private  String Lon = "Log";
    private  String Lat = "Lat";
    private  String IsUserExist = "IsUserExist";
    private String ImageBitmap = "ImageBitmap";
    private String ImageSet = "ImageSet";




    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "GsXWdY21fGx7rblb8eeGq6FEQSTfOKXP0PRwIXqn", "XQrTDPYkuBmvliA1QG5l8t7LitVU0HisHnkd1JqS");//dev app

        this.appSharedPrefs = getSharedPreferences(SHARED_NAME,
                Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public void clearAll(){
        prefsEditor.clear().commit();
    }

    public void setFirstName(String firstName) {
        prefsEditor.putString(FirstName, firstName).commit();
    }

    public String getFirstName() {
        return appSharedPrefs.getString(FirstName, "");
    }

    public void setLastName(String lastName) {
        prefsEditor.putString(LastName, lastName).commit();
    }

    public String getLastName() {
        return appSharedPrefs.getString(LastName, "");
    }

    public void setImageUrl(String imageUrl) {
        prefsEditor.putString(ImageUrl, ImageUrl).commit();
    }

    public String getImageUrl() {
        return appSharedPrefs.getString(ImageUrl, "");
    }

    public void setImageBitmap(String imageBitmap) {
        prefsEditor.putString(ImageBitmap, ImageBitmap).commit();
    }

    public String getImageBitmap() {
        return appSharedPrefs.getString(ImageBitmap, "");
    }


    public void setPostCode(String postCode) {
        prefsEditor.putString(PostCode, postCode).commit();
    }

    public String getPostCode() {
        return appSharedPrefs.getString(PostCode, "");
    }

    public void setEduLevel(String eduLevel) {
        prefsEditor.putString(EduLevel, eduLevel).commit();
    }

    public String getEduLevel() {
        return appSharedPrefs.getString(EduLevel, "");
    }
    public void setEligible(Boolean eligible) {
        prefsEditor.putBoolean(Eligible, eligible).commit();
    }

    public Boolean getEligible() {
        return appSharedPrefs.getBoolean(Eligible, false);
    }

    public void setExpROle(String expROle) {
        prefsEditor.putString(ExpROle, expROle).commit();
    }

    public String getExpROle() {
        return appSharedPrefs.getString(ExpROle, "");
    }

    public void setExpTime(String expTime) {
        prefsEditor.putString(ExpTime, expTime).commit();
    }

    public String getExpTime() {
        return appSharedPrefs.getString(ExpTime, "");
    }

    public void setExpTime1(int expTime1) {
        prefsEditor.putInt(ExpTime1, expTime1).commit();
    }

    public int getExpTime1() {
        return appSharedPrefs.getInt(ExpTime1, 0);
    }


    public void setExpTime2(int expTime2) {
        prefsEditor.putInt(ExpTime2, expTime2).commit();
    }

    public int getExpTime2() {
        return appSharedPrefs.getInt(ExpTime2, 0);
    }

    public void setExpTime3(int expTime3) {
        prefsEditor.putInt(ExpTime3, expTime3).commit();
    }

    public int getExpTime3() {
        return appSharedPrefs.getInt(ExpTime3, 0);
    }

    public void setRadius(String radius) {
        prefsEditor.putString(Radius, radius).commit();
    }

    public String getRadius() {
        return appSharedPrefs.getString(Radius, "");
    }

    public void setTotalRoles(int totalRoles) {
        prefsEditor.putInt(TotalRoles, totalRoles).commit();
    }

    public int getTotalRoles() {
        return appSharedPrefs.getInt(TotalRoles, 0);
    }

    public void setPhoneNumber(String phoneNumber){ prefsEditor.putString(PhoneNumber, phoneNumber).commit();}

    public String getPhoneNumber(){return appSharedPrefs.getString(PhoneNumber, "");}
    public void setLon(String lon){
        prefsEditor.putString(this.Lon, lon).commit();
    }
    public String getLon(){
        return appSharedPrefs.getString(this.Lon, "");
    }

    public void setLat(String lat){
        prefsEditor.putString(this.Lat, lat).commit();
    }
    public String getLat(){
        return appSharedPrefs.getString(this.Lat, "");
    }

    public void setIsUserExsit(String isUserExist){
        prefsEditor.putString(this.IsUserExist, isUserExist).commit();
    }
    public String getIsUserExsit(){
        return appSharedPrefs.getString(this.IsUserExist, "");
    }


    public void clearSharedPrefs() {
        prefsEditor.clear().commit();
    }

    public void setImageTag( boolean image){ prefsEditor.putBoolean(ImageSet,image).commit();}

    public boolean getImageTag(){return appSharedPrefs.getBoolean(ImageSet,false);}

}
