package com.algorepublic.yapjobs.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;

import java.util.ArrayList;

/**
 * Created by android on 8/3/15.
 */
public class CustomPagerAdapter extends PagerAdapter {

    ArrayList<IntroItems> introItem;

    Context mContext;
    AQuery aq;
    LayoutInflater mLayoutInflater;

    public CustomPagerAdapter(Context context, ArrayList<IntroItems> result) {
        introItem = result;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomPagerAdapter() {
        super();
    }
    @Override
    public int getCount() {

        try {
            return introItem.size();
        }catch (NullPointerException e){

            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = mLayoutInflater.inflate(R.layout.layout_intro_pager, container, false);
        aq = new AQuery(view);
        aq.id(R.id.header_text).text(introItem.get(position).getTitle()).getTextView().setTypeface(TypeFaces.get(mContext, BaseClass.Quicksand_Bold));
        aq.id(R.id.desc_text).text(introItem.get(position).getDesc()).getTextView().setTypeface(TypeFaces.get(mContext, BaseClass.Quicksand_Bold));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }
}
