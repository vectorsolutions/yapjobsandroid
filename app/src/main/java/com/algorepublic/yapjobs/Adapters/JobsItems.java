package com.algorepublic.yapjobs.Adapters;

/**
 * Created by android on 8/19/15.
 */
public class JobsItems {

    public void  setJobId(String id)
    {
        this.jobId = id;
    }
    public String  getJobId()
    {
        return jobId ;
    }
    public void  setDistance(String distance)
    {
        this.distance = distance;
    }
    public String  getDistance()
    {
        return distance ;
    }
    public void  setStartDate(String startDate)
    {
        this.startDate = startDate;
    }
    public String  getStartDate()
    {
        return startDate ;
    }
    public void  setHourlyRate(String hourlyRate)
    {
        this.hourlyRate = hourlyRate;
    }
    public String  getHourlyRate()
    {
        return hourlyRate ;
    }
    public void  setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    public String  getRoleName()
    {
        return roleName ;
    }
    public void  setRoleImage(String roleImage)
    {
        this.roleImage = roleImage;
    }
    public String  getRoleImage()
    {
        return roleImage ;
    }
    public void  setBusinessName(String businessName)
    {
        this.businessName = businessName;
    }
    public String  getBusinessName()
    {
        return businessName ;
    }
    public void setLocation(String location){ this.location = location; }
    public String getLocation(){return location;}
    public void setAboutJob(String aboutJob){this.aboutJob = aboutJob;}
    public String getAboutJob(){return aboutJob;}
    public void setResponsibility(String responsibility){this.responsibility = responsibility;}
    public String getResponsibility(){return  responsibility;}


    private String jobId ;
    private String distance ;
    private String startDate;
    private String hourlyRate;
    private String roleName;
    private String roleImage;
    private String businessName;
    private String location;
    private String aboutJob;
    private String responsibility;

}
