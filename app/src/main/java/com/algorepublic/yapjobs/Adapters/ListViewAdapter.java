package com.algorepublic.yapjobs.Adapters;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.algorepublic.yapjobs.Fragments.FragmentJobDetail;
import com.algorepublic.yapjobs.Fragments.FragmentJobs;
import com.algorepublic.yapjobs.Models.JobStatus;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;
import com.daimajia.swipe.SwipeLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ListViewAdapter extends BaseAdapter {

    ArrayList<JobsItems> arraylist = new ArrayList<JobsItems>();
    Context ctx;
    AQuery aq, aq_popUp;
    int Position;
    private LayoutInflater l_Inflater;
    BaseClass baseClass;
    ProgressDialog dialog;
    Boolean applyFlag,declineFlag;
    View popUp;
    public AlertDialog alert;


    public ListViewAdapter(Context context,ArrayList<JobsItems> result) {
        l_Inflater = LayoutInflater.from(context);
        FragmentJobs.resultitems = new ArrayList<JobsItems>();
        FragmentJobs.resultitems = result;
        arraylist.addAll(FragmentJobs.resultitems);
        this.ctx = context;
        dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait...");
        baseClass = ((BaseClass) context.getApplicationContext());
        applyFlag=true;declineFlag=true;
    }

    @Override
    public int getCount() {
        return FragmentJobs.resultitems.size();
    }

    @Override
    public Object getItem(int position) {
        return FragmentJobs.resultitems.get(position);
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = l_Inflater.inflate(R.layout.layout_jobs_items, null);
            holder.sample1 = (SwipeLayout) convertView.findViewById(R.id.sample1);
            holder.distance = (TextView) holder.sample1.findViewById(R.id.textView_distance);
            holder.startDate = (TextView) holder.sample1.findViewById(R.id.textView_date);
            holder.businessName= (TextView) holder.sample1.findViewById(R.id.textView_experience);
            holder.roleName = (TextView) holder.sample1.findViewById(R.id.textView_job_title);
            holder.hourlyRate = (TextView) holder.sample1.findViewById(R.id.textView_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        aq = new AQuery(holder.sample1);

//        aq.id(R.id.textView_job_title).getTextView().setTypeface(TypeFaces.get(ctx, BaseClass.SourceSansPro_Black));
//        aq.id(R.id.textView_experience).getTextView().setTypeface(TypeFaces.get(ctx, BaseClass.SourceSansPro_Light));
//        aq.id(R.id.textView_date).getTextView().setTypeface(TypeFaces.get(ctx, BaseClass.SourceSansPro_Light));
//        aq.id(R.id.textView_price).getTextView().setTypeface(TypeFaces.get(ctx, BaseClass.SourceSansPro_Light));
//        aq.id(R.id.textView_distance).getTextView().setTypeface(TypeFaces.get(ctx, BaseClass.SourceSansPro_Light));

        aq.id(R.id.imageView_job_image).image(Constants.BASE_DEV + FragmentJobs.resultitems.get(position).getRoleImage());
        holder.roleName.setText(FragmentJobs.resultitems.get(position).getRoleName());
        holder.businessName.setText(FragmentJobs.resultitems.get(position).getBusinessName());
        holder.startDate.setText(FragmentJobs.resultitems.get(position).getStartDate());
        holder.hourlyRate.setText(FragmentJobs.resultitems.get(position).getHourlyRate() + "ph");
        holder.distance.setText(FragmentJobs.resultitems.get(position).getDistance() + " miles");

        holder.sample1.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.sample1.addDrag(SwipeLayout.DragEdge.Left, holder.sample1.findViewById(R.id.bottom_wrapper));
        holder.sample1.addDrag(SwipeLayout.DragEdge.Right, holder.sample1.findViewById(R.id.bottom_wrapper_2));

        holder.sample1.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View v) {
                Position = position;
                dialog.show();
                new AsyncdeclineJob().execute();
            }
        });
        holder.sample1.findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View v) {
                Position = position;
                dialog.show();
                new AsyncApplyJob().execute();
            }
        });
        holder.sample1.addRevealListener(R.id.delete, new SwipeLayout.OnRevealListener() {
            @Override
            public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {
                Log.e("Dis1",fraction+"/"+distance);
//                applyFlag =false;
//                if (distance == 450 && declineFlag) {
//                    Log.e("call1","call");
//                    declineFlag =false;
//
//                    Position = position;
//                    dialog.show();
//                    new AsyncdeclineJob().execute();
//                }
            }
        });
                holder.sample1.addRevealListener(R.id.apply, new SwipeLayout.OnRevealListener() {
                    @Override
                    public void onReveal(View child, SwipeLayout.DragEdge edge, float fraction, int distance) {
                        Log.e("Dis2",fraction+ "/" + distance);
//                        declineFlag = false;
//                        if (distance == 420 && applyFlag) {
//                            Log.e("call2", "call");
//                            applyFlag = false;
//                            Position = position;
//                            dialog.show();
//                            new AsyncApplyJob().execute();
//                        }
                    }
                });
        holder.sample1.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callFragment(R.id.base_container,FragmentJobDetail.newInstance(position,FragmentJobs.resultitems),"JobsDetails");
            }
        });

        return convertView;
    }
    protected void callFragment(int containerId, android.support.v4.app.Fragment fragment, String tag){
        ((FragmentActivity) ctx).getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commit();
    }
    static class ViewHolder {
        SwipeLayout sample1;
        private TextView distance ;
        private TextView startDate;
        private TextView hourlyRate;
        private TextView roleName;
        private TextView businessName;
    }

    public void filter(String id) {
        Log.e("ok", id);
        arraylist.addAll(FragmentJobs.resultitems);
        FragmentJobs.resultitems.clear();
        for (int loop = 0; loop < arraylist.size(); loop++) {
            if (arraylist.get(loop).getJobId() != id) {
                FragmentJobs.resultitems.add(arraylist.get(loop));
            }
        }
        arraylist.clear();
        notifyDataSetChanged();
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public class AsyncApplyJob extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                Log.e("Url", Constants.BASE_URL + Constants.Apply_URL);
                response = httpClient.post(Constants.BASE_URL+Constants.Apply_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
//            Log.e("Abc", JobStatus.getInstance().apply_quota_remaining + "");

            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(result.toString());
            }catch (JSONException e){}
                Gson gson = new Gson();
                JobStatus obj ;
                obj = gson.fromJson(result.toString(),
                        JobStatus.class);
            JobStatus.getInstance().setList(obj);

            if(JobStatus.getInstance().status == 200)
            {
                Log.e("Status 200", JobStatus.getInstance().status + "");
                showDialog();

                filter(FragmentJobs.resultitems.get(Position).getJobId());
                Crouton.makeText( (FragmentActivity) ctx,"Job Applied Successfully!", Style.INFO);
            }
            if(JobStatus.getInstance().status == 402)
            {
                //popup
                Log.e("Status 402", JobStatus.getInstance().status + "");
                showDialog();
            }
            if(JobStatus.getInstance().apply_quota_remaining > 0)
            {
                applyFlag=true;
            }
            else
                applyFlag=false;

        }
    }
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public class AsyncdeclineJob extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Decline_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(result.toString());
            }catch (JSONException e){}
            Gson gson = new Gson();
            JobStatus obj ;
            obj = gson.fromJson(result.toString(),
                    JobStatus.class);
            JobStatus.getInstance().setList(obj);
            if(JobStatus.getInstance().status == 200)
            {
                filter(FragmentJobs.resultitems.get(Position).getJobId());
                declineFlag=true;
                Crouton.makeText( (FragmentActivity) ctx,"Job Declined", Style.INFO);
            }
            else
                declineFlag=false;
            if(JobStatus.getInstance().apply_quota_remaining > 0)
            {
                applyFlag=true;
            }
            else
                applyFlag=false;
        }
    }

    public String  GetJsonObject() throws JSONException {

        JSONObject object = new JSONObject();

        object.put("phone", baseClass.getPhoneNumber());
        object.put("job_id", FragmentJobs.resultitems.get(Position).getJobId());

        return object.toString();
    }

    public void showDialog(){
        popUp = l_Inflater.inflate(R.layout.popup_jobs,
                null, false);
        aq_popUp = new AQuery(popUp);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setView(popUp);
        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
        Window window = alert.getWindow();
        window.setLayout(1000, 1330);
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//
//        lp.copyFrom(alert.getWindow().getAttributes());
//        lp.width = 1000;
//        lp.height = 1000;
//        lp.x=-100;
//        lp.y=100;
//        alert.getWindow().setAttributes(lp);
        Log.e("Tag",baseClass.getImageTag()+"");
        if (JobStatus.getInstance().status == 200){
            aq_popUp.id(R.id.congrax).text("CONGRATULATIONS!");
            aq_popUp.id(R.id.number).text(JobStatus.getInstance().apply_quota_remaining);
        }
        if (JobStatus.getInstance().status == 402){
            aq_popUp.id(R.id.congrax).text("SORRY!");
            aq_popUp.id(R.id.congrax).textColor(Color.parseColor("#990000"));
            aq_popUp.id(R.id.number).text(String.valueOf(JobStatus.getInstance().apply_quota_remaining));
            aq_popUp.id(R.id.number).textColor(Color.parseColor("#990000"));
        }

        aq_popUp.id(R.id.close_dialog).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        aq_popUp.id(R.id.layout_third).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


    }
}