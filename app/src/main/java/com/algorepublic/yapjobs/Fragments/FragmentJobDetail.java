package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.Adapters.JobsItems;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.androidquery.AQuery;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by ahmad on 8/24/15.
 */
public class FragmentJobDetail extends BaseFragment {

    View view;
    BaseClass baseClass;
    static ArrayList<JobsItems> arrayList = new ArrayList<JobsItems>();
    static int position;
    static FragmentJobDetail fragmentJobDetail;
    AQuery aq;
    public GoogleMap googleMap;
    ProgressDialog dialog;
    int counter ;

    public static FragmentJobDetail newInstance(int Position, ArrayList<JobsItems> ArrayList){
        position = Position;
        arrayList = ArrayList;
        fragmentJobDetail = new FragmentJobDetail();
        return fragmentJobDetail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_details,container,false);
        baseClass = (BaseClass) getActivity().getApplicationContext();
        aq = new AQuery(view);
//        counter = position;
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        Log.e("image:", arrayList.get(position).getRoleImage());
        initilizeMap(position);
        aq.id(R.id.imageView_job_image).image("http://yapjobs-qa.herokuapp.com/" + arrayList.get(position).getRoleImage());
        aq.id(R.id.textView_job_title).text(arrayList.get(position).getRoleName());
        aq.id(R.id.textView_experience).text(arrayList.get(position).getDistance());
        aq.id(R.id.textView_date).text(arrayList.get(position).getStartDate());
        aq.id(R.id.textView_price).text(arrayList.get(position).getHourlyRate());
        aq.id(R.id.textView_date).text(arrayList.get(position).getStartDate());
        aq.id(R.id.textView_distance).text(arrayList.get(position).getDistance());
        aq.id(R.id.textView_about_desc).text(arrayList.get(position).getAboutJob());
        aq.id(R.id.cancel_action).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        aq.id(R.id.apply_job).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                new AsyncApplyJob().execute();
            }
        });
        aq.id(R.id.decline_job).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                new AsyncdeclineJob().execute();
            }
        });

        return view;
    }

    private void initilizeMap(int pos) {
//        if (googleMap == null){
            googleMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.places_mapView)).getMap();
            Log.e("Location :",arrayList.get(pos).getLocation());
            // latitude and longitude
            String[] parts = arrayList.get(pos).getLocation().split(",");
            double latitude = Double.valueOf(parts[0]);
            double longitude = Double.valueOf(parts[1]);
// create marker
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude))
                    .title(arrayList.get(pos).getRoleName());
// adding marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(17).build();
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_icon));

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            googleMap.addMarker(marker);
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initilizeMap(position);
    }


    public class AsyncApplyJob extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                Log.e("Url",Constants.BASE_URL+Constants.Apply_URL );
                response = httpClient.post(Constants.BASE_URL+Constants.Apply_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            Log.e("ok", result.toString() + "/" + arrayList.get(position).getJobId());

            if(result.contains("Success"))
            {
                Crouton.makeText( getActivity() ,"Job Applied Successfully!", Style.INFO);
                position++;
                updateUi();
            }
            if (result.contains("402")){
                Crouton.makeText( getActivity() ,"Your limit for Applying Job is Exceeded", Style.ALERT).show();
            }
        }
    }

    public class AsyncdeclineJob extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Decline_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            if(result.contains("Declined"))
            {
                Crouton.makeText( getActivity(),"Job Declined", Style.INFO);
                position++;
                updateUi();
            }
        }
    }

    public String  GetJsonObject() throws JSONException {

        JSONObject object = new JSONObject();

        object.put("phone", baseClass.getPhoneNumber());
        object.put("job_id", arrayList.get(position).getJobId());

        return object.toString();
    }

    public void updateUi(){
        aq.id(R.id.imageView_job_image).image("http://yapjobs-qa.herokuapp.com/" + arrayList.get(position).getRoleImage());
        aq.id(R.id.textView_job_title).text(arrayList.get(position).getRoleName());
        aq.id(R.id.textView_experience).text(arrayList.get(position).getDistance());
        aq.id(R.id.textView_date).text(arrayList.get(position).getStartDate());
        aq.id(R.id.textView_price).text(arrayList.get(position).getHourlyRate());
        aq.id(R.id.textView_date).text(arrayList.get(position).getStartDate());
        aq.id(R.id.textView_distance).text(arrayList.get(position).getDistance());
        aq.id(R.id.textView_about_desc).text(arrayList.get(position).getAboutJob());
        googleMap.clear();
        initilizeMap(position);
    }

}
