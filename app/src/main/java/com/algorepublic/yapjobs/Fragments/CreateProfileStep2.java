package com.algorepublic.yapjobs.Fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.algorepublic.yapjobs.DB.DaoMaster;
import com.algorepublic.yapjobs.DB.DaoSession;
import com.algorepublic.yapjobs.DB.Roles;
import com.algorepublic.yapjobs.DB.RolesDao;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

import Adapters.AdapterJobRoles;
import Adapters.JobsRolesItems;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateProfileStep2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateProfileStep2 extends BaseFragment {

    AQuery aq;
    BaseClass baseClass;
    ProgressDialog dialog;
    public static GridView gridView;
    public static CreateProfileStep2 newInstance() {
        CreateProfileStep2 fragment = new CreateProfileStep2();
        return fragment;
    }

    public CreateProfileStep2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_create_profile_step2, container, false);
        aq = new AQuery(view);
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        gridView = (GridView) view.findViewById(R.id.gridView_role);
        aq.id(R.id.step2).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.text).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.text1).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.text2).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.text3).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        baseClass = ((BaseClass) getActivity().getApplicationContext());
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFragment(R.id.container,CreateProfileStep3.newInstance(),"Step3");
            }
        });

        dialog.show();
        new AsyncTry().execute();

        return view;
    }
    public class AsyncTry extends AsyncTask<Void, Void, String> {

        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {

            httpClient = new GenericHttpClient();
            try {
                response = httpClient.get(Constants.BASE_URL + Constants.JobsRoles_URL);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            PopulateModel(result);
        }
    }
    private void PopulateModel (String json) {
        JSONArray jsonArray;
        ArrayList<JobsRolesItems> arrayList = new ArrayList<JobsRolesItems>();
        try {
            jsonArray = new JSONArray(json);
            for (int loop = 0; loop <= jsonArray.length(); loop++) {
                JobsRolesItems jobsItems = new JobsRolesItems();
                jobsItems.setJobId(String.valueOf(jsonArray.getJSONObject(loop).getInt("id")));
                jobsItems.setName(String.valueOf(jsonArray.getJSONObject(loop).getString("name")));
                jobsItems.setImagUnselected(String.valueOf(jsonArray.getJSONObject(loop).getString("image")));
                jobsItems.setImagSelected(SplitRole(String.valueOf(jsonArray.getJSONObject(loop).getString("image"))) + "_unselected.png");
                jobsItems.setSlug(String.valueOf(jsonArray.getJSONObject(loop).getString("slug")));
                arrayList.add(jobsItems);
            }
        } catch (Exception e) {
        }
        AdapterJobRoles adapterJobRoles = new AdapterJobRoles(getActivity(), arrayList);
        gridView.setAdapter(adapterJobRoles);

    }

    public String SplitRole(String role)
    {
        String[] title = role.split("\\.");
        return title[0];
    }
}
