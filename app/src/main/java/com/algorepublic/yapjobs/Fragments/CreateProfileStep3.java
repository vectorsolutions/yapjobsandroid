package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.BaseActivity;
import com.algorepublic.yapjobs.DB.DaoMaster;
import com.algorepublic.yapjobs.DB.DaoSession;
import com.algorepublic.yapjobs.DB.RoleList;
import com.algorepublic.yapjobs.DB.Roles;
import com.algorepublic.yapjobs.DB.RolesDao;
import com.algorepublic.yapjobs.Models.RegisterUserModel;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GPSTracker;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.techery.progresshint.ProgressHintDelegate.SeekBarHintAdapter;


/**
 * Created by ahmad on 8/5/15.
 */
public class CreateProfileStep3 extends BaseFragment {

    View view;
    AQuery aq;
    String val1,val2,val3;
    BaseClass baseClass;
    ArrayList<RoleList> lists;
    static GPSTracker gps;
    ProgressDialog dialog;



    public static CreateProfileStep3 newInstance(){
        CreateProfileStep3 fragment = new CreateProfileStep3();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create_profile_step3,container,false);
        aq = new AQuery(view);
        gps = new GPSTracker(getActivity());
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        aq.id(R.id.text).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        baseClass = ((BaseClass)getActivity().getApplicationContext());
        GetGPS();
        lists = GetRolesFromDB();
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lists.size() == 1) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1);
                }
                else if(lists.size() == 2) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1+" "+val2);
                }
                else if(lists.size() == 3) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1+" "+val2+" "+val3);
                }
                dialog.show();
                new AsyncTry().execute();
            }
        });

        if(lists.size()==1)
            ShowSeekBar1();
        if(lists.size()==2) {
            ShowSeekBar1();
            ShowSeekBar2();
        }
        if(lists.size()==3)
        {
            ShowSeekBar1();
            ShowSeekBar2();
            ShowSeekBar3();
        }

        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical1)).getHintDelegate()
                .setHintAdapter(new SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val1 = String.valueOf(progress);
                        baseClass.setExpTime1(progress);
                        return String.valueOf(progress);
                    }
                });
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical2)).getHintDelegate()
                .setHintAdapter(new SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val2 = String.valueOf(progress);
                        baseClass.setExpTime2(progress);
                        return String.valueOf(progress);
                    }
                });
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical3)).getHintDelegate()
                .setHintAdapter(new SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val3 = String.valueOf(progress);
                        baseClass.setExpTime3(progress);
                        return String.valueOf(progress);
                    }
                });
        return view;
    }

    public class AsyncTry extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Register_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            PopulateModel(result);
        }
    }
    private void PopulateModel (String json) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json.toString());
            Gson gson = new Gson();
            RegisterUserModel obj ;
            obj = gson.fromJson(jsonObj.toString(),
                    RegisterUserModel.class);
            RegisterUserModel.getInstance().setList(obj);
            Log.e("status","/"+RegisterUserModel.getInstance().status+RegisterUserModel.getInstance().message);
            if(RegisterUserModel.getInstance().status == 200)
            {
                baseClass.setIsUserExsit("1");
                getActivity().finish();
                startActivity(new Intent(getActivity(), BaseActivity.class));
            }
            else
                baseClass.setIsUserExsit("0");
        }catch (Exception e){}
    }
    private void ShowSeekBar1() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical1)).
                setProgress(baseClass.getExpTime1());
        view.findViewById(R.id.layout_experience1).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical1).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_one).text("Experince as " + lists.get(0).getRoleName());
    }
    private void ShowSeekBar2() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical2)).
                setProgress(baseClass.getExpTime2());
        view.findViewById(R.id.layout_experience2).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical2).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_two).text("Experince as " + lists.get(1).getRoleName());
    }
    private void ShowSeekBar3() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical3)).
                setProgress(baseClass.getExpTime3());
        view.findViewById(R.id.layout_experience3).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical3).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_three).text("Experince as " + lists.get(2).getRoleName());
    }
    private ArrayList<RoleList> GetRolesFromDB( ) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                getActivity(), "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();
        RolesDao rolesDao = daoSession.getRolesDao();
        List<Roles> temp = rolesDao.queryBuilder().list();
        StringBuilder builder = new StringBuilder();
        for(int loop=0;loop<temp.size();loop++)
            builder.append(temp.get(loop).getRole_slug() + " ");
        baseClass.setExpROle(builder.toString().trim());
        ArrayList<RoleList> roleLists = new ArrayList<RoleList>();
        if(temp.size() !=0)
            for(int loop=0;loop<temp.size();loop++) {
                RoleList roleList = new RoleList();
                roleList.setRoleName(temp.get(loop).getRole_name());
                roleLists.add(roleList);
            }

        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
        return roleLists;
    }


    public String  GetJsonObject() throws JSONException {

        Log.e("String",baseClass.getPhoneNumber()+"/"+baseClass.getExpROle()+"/"+baseClass.getExpTime()+"/"+baseClass.getTotalRoles()
                +"/"+baseClass.getEduLevel()+"/"+baseClass.getEligible()+"/"+ baseClass.getRadius()+"/"+baseClass.getLat()+","+baseClass.getLon());

        JSONObject object = new JSONObject();
        JSONObject userObj = new JSONObject();

        object.put("email", baseClass.getFirstName() + "@example.com");
        object.put("first_name", baseClass.getFirstName());
        object.put("last_name", baseClass.getLastName());
        object.put("password", "321321322");
        object.put("location", baseClass.getLat()+","+baseClass.getLon());
        object.put("date_of_birth", "1983-06-12");
        object.put("postcode", "SE1 1TL");
        object.put("phone", baseClass.getPhoneNumber());
        object.put("city", "Karachi");
        object.put("profile_image", "http,//www.matchninja.com/wp-content/uploads/2014/08/online-dating-profile.jpg");
        object.put("about_me", "I'm 23");
        object.put("interested_in", baseClass.getExpROle());
        object.put("experiences", baseClass.getExpTime());
        object.put("user_type", "job_seeker");
        object.put("experience", baseClass.getTotalRoles());
        object.put("education_level", baseClass.getEduLevel());
        object.put("allowed_working_in_uk", baseClass.getEligible());
        object.put("search_job_radius", "100000000");

        userObj.put("user", object);

        return userObj.toString();
    }
    public void GetGPS() {
        if (gps.canGetLocation()) {
            baseClass.setLat(String.valueOf(gps.getLatitude()));
            baseClass.setLon(String.valueOf(gps.getLongitude()));
        }
        else{
            gps.showSettingsAlert();
        }
    }

}
