package com.algorepublic.yapjobs.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.algorepublic.yapjobs.Models.PostcodeModel;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
// commented due to url error
//import com.algorepublic.yapjobs.amazonaws.Constants;
//import com.algorepublic.yapjobs.amazonaws.Util;
//import com.algorepublic.yapjobs.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
//import com.algorepublic.yapjobs.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
//import com.algorepublic.yapjobs.amazonaws.mobileconnectors.s3.transferutility.TransferState;
//import com.algorepublic.yapjobs.amazonaws.mobileconnectors.s3.transferutility.TransferType;
//import com.algorepublic.yapjobs.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.androidquery.AQuery;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

//import com.google.code.linkedinapi.client.LinkedInApiClient;
//import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
//import com.google.code.linkedinapi.client.enumeration.ProfileField;
//import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
//import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
//import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
//import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
//import com.google.code.linkedinapi.schema.Person;

/**
 * Use the {@link CreateProfileStep1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateProfileStep1 extends BaseFragment {
    // commented due to url error
//    private TransferUtility transferUtility;
//    private List<TransferObserver> observers;
    private ArrayList<HashMap<String, Object>> transferRecordMaps;
    public static final String TAG = "CreateProfileStep1";
    AQuery aq;
    BaseClass baseClass;
    CallbackManager callbackManager;
    ImageView img_logo;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    private Intent pictureActionIntent = null;
    Bitmap bitmap;

    String selectedImagePath;
    String var;
//    final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
//            .getInstance().createLinkedInOAuthService(
//                    Config.LINKEDIN_CONSUMER_KEY,Config.LINKEDIN_CONSUMER_SECRET);
//    final LinkedInApiClientFactory factory = LinkedInApiClientFactory
//            .newInstance(Config.LINKEDIN_CONSUMER_KEY,
//                    Config.LINKEDIN_CONSUMER_SECRET);
//    LinkedInRequestToken liToken;
//    LinkedInApiClient client;
//    LinkedInAccessToken accessToken = null;
    Bitmap thumbnail;

    public static CreateProfileStep1 newInstance() {
        CreateProfileStep1 fragment = new CreateProfileStep1();
        return fragment;
    }

    public CreateProfileStep1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // commented due to url erroe
//        transferUtility = Util.getTransferUtility(getActivity());
        initData();
    }
    private void initData() {

        transferRecordMaps = new ArrayList<HashMap<String, Object>>();

        // Use TransferUtility to get all upload transfers.
        // commented due to url error
//        observers = transferUtility.getTransfersWithType(TransferType.UPLOAD);
//        for (TransferObserver observer : observers) {
//
//            // For each transfer we will will create an entry in
//            // transferRecordMaps which will display
//            // as a single row in the UI
//            HashMap<String, Object> map = new HashMap<String, Object>();
//            Util.fillMap(map, observer, false);
//            transferRecordMaps.add(map);
//
//            // We only care about updates to transfers that are in a
//            // non-terminal state
//            if (!TransferState.COMPLETED.equals(observer.getState())
//                    && !TransferState.FAILED.equals(observer.getState())
//                    && !TransferState.CANCELED.equals(observer.getState())) {
//
//                observer.setTransferListener(new UploadListener());
//            }
//        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_create_profile_step1, container, false);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        aq= new AQuery(view);
        aq.id(R.id.step1).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.text).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.edu_level).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.school).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.college).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.university).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.eligible).getCheckBox().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        img_logo = (ImageView) view.findViewById(R.id.profile_image);
        var = aq.id(R.id.post_code).getEditText().getText().toString();
        baseClass = ((BaseClass) getActivity().getApplicationContext());
        if(!baseClass.getFirstName().isEmpty()){
            aq.id(R.id.facebook_button).background(R.drawable.facebook_button_green);
        }
        final LoginButton loginButton = (LoginButton) view.findViewById(R.id.facebook);
        aq.id(R.id.facebook_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick(); // fake button to use facebook login button ;)
            }
        });
        loginButton.setReadPermissions("public_profile");
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("test", loginResult.getAccessToken().getToken());
                try {
                    UpdateUI(Profile.getCurrentProfile());
                }catch (Exception e){
                    e.printStackTrace();
                    logout();
                    LoginManager.getInstance().logOut(); //having issue first time logging in...throws exception
                    loginButton.performClick();
                }
            }
            @Override
            public void onCancel() {
                Crouton.makeText(getActivity(),"You cancelled", Style.INFO).show();
            }
            @Override
            public void onError(FacebookException exception) {
                Crouton.makeText(getActivity(),"Something went wrong!", Style.INFO).show();
                Log.e("error", exception.getMessage());
            }
        });

    aq.id(R.id.profile_image).clicked(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            if (Build.VERSION.SDK_INT >= 19) {
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            } else {
                intent.setAction(Intent.ACTION_GET_CONTENT);
            }

            intent.setType("image/*");
            startActivityForResult(intent, 0);
            showDialog();
        }
    });

        aq.id(R.id.linked_in).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                linkedInLogin();
            }
        });
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validated()){
                    PutValues();
                    new AsyncCheckPostcode().execute();
                    callFragment(R.id.container, CreateProfileStep2.newInstance(), "Step2");
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path=null;
        if (requestCode == GALLERY_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    // our BitmapDrawable for the thumbnail
                    BitmapDrawable bmpDrawable = null;
                    // try to retrieve the image using the data from the intent
                    Cursor cursor = getActivity().getContentResolver().query(data.getData(),
                            null, null, null, null);
                    if (cursor != null) {

                        cursor.moveToFirst();

                        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                        path = cursor.getString(idx);

                    } else {

                        path =  data.getData().getPath();
                    }

                } else {
                    Toast.makeText(getActivity(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra("data")) {

                    // retrieve the bitmap from the intent
                    bitmap = (Bitmap) data.getExtras().get("data");


                    Cursor cursor = getActivity().getContentResolver()
                            .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    new String[]{
                                            MediaStore.Images.Media.DATA,
                                            MediaStore.Images.Media.DATE_ADDED,
                                            MediaStore.Images.ImageColumns.ORIENTATION},
                                    MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            Uri uri = Uri.parse(cursor.getString(cursor
                                    .getColumnIndex(MediaStore.Images.Media.DATA)));
                            path = uri.toString();
                        } while (cursor.moveToNext());
                        cursor.close();
                    }
                } else if (data.getExtras() == null) {

                    Toast.makeText(getActivity(),
                            "No extras to retrieve!", Toast.LENGTH_SHORT)
                            .show();

                    path =  data.getData().getPath();

                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        beginUpload(path);
    }
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(getActivity(), "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        // commented due to url error
//        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
//                file);
//        observers.add(observer);
//        HashMap<String, Object> map = new HashMap<String, Object>();
//        Util.fillMap(map, observer, false);
//        transferRecordMaps.add(map);
//        observer.setTransferListener(new UploadListener());
    }
    private boolean checkSelectionExists() {
        return aq.id(R.id.school).isChecked() ||
                aq.id(R.id.college).isChecked() ||
                aq.id(R.id.university).isChecked();
    }

    private void PutValues() {
        baseClass.setFirstName(aq.id(R.id.first_name).getText().toString());
        baseClass.setLastName(aq.id(R.id.last_name).getText().toString());
        baseClass.setPostCode(aq.id(R.id.post_code).getText().toString());
        if(aq.id(R.id.school).isChecked())
            baseClass.setEduLevel("1");
        if(aq.id(R.id.college).isChecked())
            baseClass.setEduLevel("2");
        if(aq.id(R.id.university).isChecked())
            baseClass.setEduLevel("3");
        if(aq.id(R.id.eligible).isChecked())
            baseClass.setEligible(true);
        else
            baseClass.setEligible(false);

    }
    protected void callFragment(int containerId, Fragment fragment, String tag){
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commit();
    }
    public void UpdateUI(Profile profile) {
        aq.id(R.id.facebook_button).background(R.drawable.facebook_button_green);
        baseClass.setFirstName(profile.getFirstName());
        baseClass.setLastName(profile.getLastName());
        baseClass.setImageUrl(String.valueOf(profile.getProfilePictureUri(576, 576)));
        aq.id(R.id.first_name).text(profile.getFirstName());
        aq.id(R.id.last_name).text(profile.getLastName());
        aq.id(R.id.profile_image).image(String.valueOf(profile.getProfilePictureUri(576, 576)));
    }

//    public void updateUILinkedin(Person profile){
//        aq.id(R.id.linked_in).background(R.drawable.linkedin_button_green);
//        baseClass.setFirstName(profile.getFirstName());
//        baseClass.setLastName(profile.getLastName());
//        baseClass.setImageUrl(String.valueOf(profile.getPictureUrl()));
//        aq.id(R.id.first_name).text(profile.getFirstName());
//        aq.id(R.id.last_name).text(profile.getLastName());
//        aq.id(R.id.profile_image).image(String.valueOf(profile.getPictureUrl()));
//    }

    private boolean validated(){
        if (aq.id(R.id.first_name).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter first name", Style.ALERT).show();
            return false;
        }
        if (aq.id(R.id.last_name).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter last name", Style.ALERT).show();
            return false;
        }

        if (aq.id(R.id.post_code).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter postcode", Style.ALERT).show();
            return false;
        }
        if (!aq.id(R.id.post_code).getEditText().getText().toString().contains(" ")) {
            Crouton.makeText(getActivity(), "Please enter correct UK postcode", Style.ALERT).show();
            return false;
        }
        if (!checkSelectionExists()) {
            Crouton.makeText(getActivity(), "Please choose at least one option from education level", Style.ALERT).show();
            return false;
        }
        if(aq.id(R.id.eligible).isChecked()){
            Crouton.makeText(getActivity(), "You are not allowed to work in UK", Style.ALERT).show();
            return false;
        }
        return true;
    }

    private void logout(){
        aq.id(R.id.facebook_button).background(R.drawable.facebook_button_gray);
        baseClass.clearAll();
    }

//    private void linkedInLogin() {
//        ProgressDialog progressDialog = new ProgressDialog(
//                getActivity());
//
//        LinkedinDialog d = new LinkedinDialog(getActivity(),
//                progressDialog);
//
//        d.show();
//
//        // set call back listener to get oauth_verifier value
//        d.setVerifierListener(new OnVerifyListener() {
//            @SuppressLint("NewApi")
//            public void onVerify(String verifier) {
//                try {
//                    Log.i("LinkedinSample", "verifier: " + verifier);
//
//                    accessToken = LinkedinDialog.oAuthService
//                            .getOAuthAccessToken(LinkedinDialog.liToken,
//                                    verifier);
//                    LinkedinDialog.factory.createLinkedInApiClient(accessToken);
//                    client = factory.createLinkedInApiClient(accessToken);
//                    // client.postNetworkUpdate("Testing by Mukesh!!! LinkedIn wall post from Android app");
//                    Log.i("LinkedinSample",
//                            "ln_access_token: " + accessToken.getToken());
//                    Log.i("LinkedinSample",
//                            "ln_access_token: " + accessToken.getTokenSecret());
//                    //Person p = client.getProfileForCurrentUser();
//                    Person p = 	client.getProfileForCurrentUser(EnumSet.of(
//                            ProfileField.ID, ProfileField.FIRST_NAME,
//                            ProfileField.PHONE_NUMBERS, ProfileField.LAST_NAME,
//                            ProfileField.HEADLINE, ProfileField.INDUSTRY,
//                            ProfileField.PICTURE_URL, ProfileField.DATE_OF_BIRTH,
//                            ProfileField.LOCATION_NAME, ProfileField.MAIN_ADDRESS,
//                            ProfileField.LOCATION_COUNTRY));
//
//                    if(p != null) {
//                        updateUILinkedin(p);
//                    }
//
//                } catch (Exception e) {
//                    Log.i("LinkedinSample", "error to get verifier");
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        // set progress dialog
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(true);
//        progressDialog.show();
//    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.DEFAULT);
        return imgString;
    }

    public class AsyncCheckPostcode extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {
                httpClient = new GenericHttpClient();
                response = httpClient.get("https://api.postcodes.io/postcodes/" +var +"/validate");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
//            dialog.dismiss();
            Crouton.makeText(getActivity(),"Profile Updated",Style.CONFIRM).show();
            PopulateModel2(result);
        }
    }
    private void PopulateModel2 (String json) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json.toString());
            Gson gson = new Gson();
            PostcodeModel obj ;
            obj = gson.fromJson(jsonObj.toString(),
                    PostcodeModel.class);
            PostcodeModel.getInstance().setList(obj);
            Log.e("status","/"+PostcodeModel.getInstance().status+PostcodeModel.getInstance().result);
            if(PostcodeModel.getInstance().status == 200)
            {

            }
            if (PostcodeModel.getInstance().result .equals(false)){
                Crouton.makeText(getActivity(),"Not a valid UK postcode",Style.ALERT).show();
                return;
            }
        }catch (Exception e){}
    }

    public void showDialog(){
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_popup);
        dialog.show();
//        baseClass.setLocalPopup(true);
        TextView text = (TextView) dialog.findViewById(R.id.textView1);
        TextView textView = (TextView) dialog.findViewById(R.id.textView);
        Button remove = (Button) dialog.findViewById(R.id.remove);
//        textView.setTypeface(TypeFaces.get(getActivity(), BaseClass.BenchNine_TYPEFACE));
//        text.setText(R.string.popup_text);
        Log.e("Tag",baseClass.getImageTag()+"");
        if (baseClass.getImageTag())
            remove.setVisibility(View.VISIBLE);
        Button camera = (Button) dialog.findViewById(R.id.camera);
        Button gallery = (Button) dialog.findViewById(R.id.gallery);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureActionIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(pictureActionIntent,
                        CAMERA_REQUEST);
                dialog.dismiss();
//                 Crouton.makeText(getActivity(), "Book is saved locally", Style.ALERT).show();

            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pictureActionIntent = new Intent(
                        Intent.ACTION_GET_CONTENT, null);
                pictureActionIntent.setType("image/*");
                pictureActionIntent.putExtra("return-data", true);
                startActivityForResult(pictureActionIntent,
                        GALLERY_PICTURE);
                dialog.dismiss();
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseClass.setImageTag(false);
                img_logo.setImageBitmap(null);
                dialog.dismiss();
            }
        });

    }

    // commented due to url error

//    private class UploadListener implements TransferListener {
//
//        // Simply updates the UI list when notified.
//        @Override
//        public void onError(int id, Exception e) {
//            Log.e(TAG, "Error during upload: " + id, e);
//            //updateList();
//        }
//
//        @Override
//        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//           // updateList();
//        }
//
//        @Override
//        public void onStateChanged(int id, TransferState newState) {
//            //newState.name();
//            //updateList();
//        }
//    }
}
