package com.algorepublic.yapjobs.Fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.HashMap;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentVerifyDevice#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentVerifyDevice extends BaseFragment {

    AQuery aq;
    BaseClass baseClass;
    ProgressDialog dialog;

    public static FragmentVerifyDevice newInstance() {
        FragmentVerifyDevice fragment = new FragmentVerifyDevice();
        return fragment;
    }

    public FragmentVerifyDevice() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_verify_device, container, false);
        aq= new AQuery(view);
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        baseClass = (BaseClass) getActivity().getApplicationContext();
        aq.id(R.id.phone_no).getTextView().requestFocus();
        aq.id(R.id.title).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.desc_text).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aq.id(R.id.phone_code).getEditText().getText().toString().equals("")) {
                    Crouton.makeText(getActivity(), "Please Enter Phone Code", Style.ALERT).show();
                    return;
                }
                if (aq.id(R.id.phone_no).getEditText().getText().toString().equals("")) {
                    Crouton.makeText(getActivity(), "Please Enter Phone Number", Style.ALERT).show();
                    return;
                }
                addPhoneNumber(aq.id(R.id.phone_code).getEditText().getText().toString(),
                        aq.id(R.id.phone_no).getEditText().getText().toString());

                toggleKeyboard();
                dialog.show();
                UserLogin(baseClass.getPhoneNumber(), baseClass.getPhoneNumber(), new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {

                        if (e == null) {
                            ConfirmRegister();
                        } else {
                            e.printStackTrace();
                            UserRegister(baseClass.getPhoneNumber(), new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        UserLogin(baseClass.getPhoneNumber(), baseClass.getPhoneNumber(), new LogInCallback() {
                                            @Override
                                            public void done(ParseUser parseUser, ParseException e) {
                                                if (e == null)
                                                    ConfirmRegister();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        return view;
    }

    public void ConfirmRegister()
    {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("phoneNumber", baseClass.getPhoneNumber());
        ParseCloud.callFunctionInBackground("sendVerificationCode", params, new FunctionCallback<String>() {
            public void done(String code, ParseException e) {
                if (e == null) {
                    callFragment(R.id.container, FragmentVerificationCode.newInstance(), "VerifyCode");
                    Crouton.makeText(getActivity(), "A verification code is sent to your phone number", Style.ALERT).show();
                }else{
                    Crouton.makeText(getActivity(),e.getMessage(),Style.INFO).show();
                }
                dialog.dismiss();
            }
        });
    }
    public void addPhoneNumber(String code, String Number){
        String phoneNumber = code + Number;
        baseClass.setPhoneNumber(phoneNumber);
        Log.e("PhoneNum", baseClass.getPhoneNumber());
    }
    public void UserRegister( String phoneno, SignUpCallback callback) {
        ParseUser user;
        user = new ParseUser();
        user.setUsername(phoneno);
        user.setPassword(phoneno);
        user.signUpInBackground(callback);

    }
    public void UserLogin(String username, String password, LogInCallback callback) {
        ParseUser user = new ParseUser();
        user.logInInBackground(username, password, callback);
    }
    protected void callFragment(int containerId, Fragment fragment, String tag){
        getActivity().getFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

}
