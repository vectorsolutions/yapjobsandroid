package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.BaseActivity;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by android on 8/3/15.
 */
public class FragmentVerificationCode extends BaseFragment {

    AQuery aq;
    BaseClass baseClass;
    ProgressDialog dialog;
    public static FragmentVerificationCode newInstance() {
        FragmentVerificationCode fragment = new FragmentVerificationCode();
        return fragment;
    }

    public FragmentVerificationCode() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_verification_code, container, false);
        aq= new AQuery(view);
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        baseClass= ((BaseClass)getActivity().getApplicationContext());

        aq.id(R.id.title).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.desc_text).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.save).getButton().setTypeface(TypeFaces.get(getActivity(), BaseClass.Quicksand_Bold));
        aq.id(R.id.code1).getTextView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (aq.id(R.id.code1).getText().length() == 1) {
                    aq.id(R.id.code2).getTextView().requestFocus();
                }
                return false;
            }
        });
        aq.id(R.id.code2).getTextView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(aq.id(R.id.code2).getText().length()== 1)
                {
                    aq.id(R.id.code3).getTextView().requestFocus();
                }
                return false;
            }
        });
        aq.id(R.id.code3).getTextView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(aq.id(R.id.code3).getText().length()== 1)
                {
                    aq.id(R.id.code4).getTextView().requestFocus();
                }
                return false;
            }
        });
        aq.id(R.id.code4).getTextView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                callFragment(R.id.container, CreateProfileStep1.newInstance(), "Step1");
                if(aq.id(R.id.code4).getText().length()== 1)
                {
                    toggleKeyboard();
                    dialog.show();
                    HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("phoneVerificationCode", aq.id(R.id.code1).getText().toString() + aq.id(R.id.code2).getText().toString()
                            + aq.id(R.id.code3).getText().toString() + aq.id(R.id.code4).getText().toString());
                    ParseCloud.callFunctionInBackground("verifyPhoneNumber", params, new FunctionCallback<String>() {
                        public void done(String code, ParseException e) {
                            if (e == null) {
                                ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                                installation.put("userName", baseClass.getPhoneNumber());
                                installation.saveInBackground();
                                // deleteUserFromParse();
                                new CheckForUser().execute();
                            }
                        }
                    });

                }
                return false;
            }
        });
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFragment(R.id.container, FragmentVerifyDevice.newInstance(), "VerifyDevice");
            }
        });
        return view;
    }
//    public void DeleteUserFromParse(final DeleteCallback callback)
//    {
//        ParseQuery query = new ParseQuery("_User");
//        query.whereEqualTo("username", baseClass.getPhoneNumber());
//        query.getFirstInBackground(new GetCallback() {
//            @Override
//            public void done(ParseObject parseObject, ParseException e) {
//                parseObject.deleteInBackground(callback);
//            }
//        });
//    }
public class CheckForUser extends AsyncTask<Void, Void, String> {
    GenericHttpClient httpClient;
    String response= null;
    @Override
    protected String doInBackground(Void... voids) {

        httpClient = new GenericHttpClient();
        try {
            String phoneno = baseClass.getPhoneNumber().replace("+","%2B");
            String url = Constants.BASE_URL + Constants.Jobs_URL + "phone=" + phoneno + "&page=1&size=1000000000";
            Log.e("url", url);
            response = httpClient.get(Constants.BASE_URL + Constants.Profile_URL + "phone=" + phoneno + "&page=1&size=100000");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
    @Override
    protected void onPostExecute(String result){
        dialog.dismiss();
        if (result.contains("200")) {
            baseClass.setIsUserExsit("1");
            startActivity(new Intent(getActivity(), BaseActivity.class));
            getActivity().finish();
        }
        else {
            baseClass.setIsUserExsit("0");
            callFragment(R.id.container, CreateProfileStep1.newInstance(), "Step1");
        }

    }
}
}
