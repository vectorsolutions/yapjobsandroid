package com.algorepublic.yapjobs.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class BaseFragment extends Fragment {

    protected BaseClass baseClass;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_base, container, false);
        baseClass = (BaseClass) getActivity().getApplicationContext();
        return view;
    }

    protected void callFragment(int containerId, Fragment fragment, String tag){
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(containerId,fragment,tag)
                .commit();
    }

    protected void deleteUserFromParse() {



        ParseQuery query = new ParseQuery("_User");
        baseClass = (BaseClass) getActivity().getApplicationContext();
        query.whereEqualTo("username", baseClass.getPhoneNumber());
        query.getFirstInBackground(new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                try {
                    parseObject.deleteInBackground();
                } catch (NullPointerException e1) {
                }
            }
        });
    }

    protected void toggleKeyboard(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


}
