package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.BaseActivity;
import com.algorepublic.yapjobs.Models.PostcodeModel;
import com.algorepublic.yapjobs.Models.RegisterUserModel;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by ahmad on 8/26/15.
 */
public class FragmentAboutMe extends BaseFragment {

    View view;
    AQuery aq;
    CallbackManager callbackManager;
    BaseClass baseClass;
    ProgressDialog dialog;
    String var;

    public static FragmentAboutMe newInstance(){
        FragmentAboutMe fragmentAboutMe = new FragmentAboutMe();
        return fragmentAboutMe;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_aboutme,container,false);
        aq = new AQuery(view);
        baseClass = (BaseClass) getActivity().getApplicationContext();
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");

        aq.id(R.id.first_or).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.second_or).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.about_me).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Black));
        aq.id(R.id.education).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.school).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Light));
        aq.id(R.id.college).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Light));
        aq.id(R.id.university).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Light));

        if (!baseClass.getFirstName().equals(""))
            aq.id(R.id.first_name).text(baseClass.getFirstName());
        if (!baseClass.getLastName().equals(""))
            aq.id(R.id.last_name).text(baseClass.getLastName());
        if (!baseClass.getPostCode().equals(""))
            aq.id(R.id.post_code).text(baseClass.getPostCode());
        if (baseClass.getEduLevel().equals("1"))
            aq.id(R.id.school).checked(true);
        if (baseClass.getEduLevel().equals("2"))
            aq.id(R.id.college).checked(true);
        if (baseClass.getEduLevel().equals("3"))
            aq.id(R.id.university).checked(true);
        aq.id(R.id.close_dialog).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        if(!baseClass.getFirstName().isEmpty()){
            aq.id(R.id.facebook_button).background(R.drawable.facebook_button_green);
        }
        final LoginButton loginButton = (LoginButton) view.findViewById(R.id.facebook);
        aq.id(R.id.facebook_button).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick(); // fake button to use facebook login button ;)
            }
        });
        loginButton.setReadPermissions("public_profile");
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("test", loginResult.getAccessToken().getToken());
                try {
                    UpdateUI(Profile.getCurrentProfile());
                }catch (Exception e){
                    logout();
                    LoginManager.getInstance().logOut(); //having issue first time logging in...throws exception
                    loginButton.performClick();
                }

            }

            @Override
            public void onCancel() {
                // App code
                Crouton.makeText(getActivity(), "You cancelled", Style.INFO).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Crouton.makeText(getActivity(),"Something went wrong!", Style.INFO).show();
                Log.e("error", exception.getMessage());
            }
        });

        aq.id(R.id.linked_in).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                linkedInLogin();
            }
        });
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validated()){
                    PutValues();
                    dialog.show();
                    var = aq.id(R.id.post_code).getEditText().getText().toString().replace(" ","%20");
                    new AsyncCheckPostcode().execute();
                    new AsyncUpdateUser().execute();
//                    callFragment(R.id.container, CreateProfileStep2.newInstance(), "Step2");
                }
            }
        });

        return view;
    }

    public class AsyncCheckPostcode extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {
                httpClient = new GenericHttpClient();
                response = httpClient.get("https://api.postcodes.io/postcodes/"+var+"/validate");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            Crouton.makeText(getActivity(),"Profile Updated",Style.INFO).show();
            PopulateModel2(result);
        }
    }
    private void PopulateModel2 (String json) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json.toString());
            Gson gson = new Gson();
            PostcodeModel obj ;
            obj = gson.fromJson(jsonObj.toString(),
                    PostcodeModel.class);
            PostcodeModel.getInstance().setList(obj);
            Log.e("status","/"+PostcodeModel.getInstance().status+PostcodeModel.getInstance().result);
            if(PostcodeModel.getInstance().status == 200)
            {

            }
            if (PostcodeModel.getInstance().result .equals(false)){
                Crouton.makeText(getActivity(),"Not A Valid UK Passcode",Style.ALERT).show();
                return;
            }
            if (PostcodeModel.getInstance().error.equals("Postcode not found")){
                Crouton.makeText(getActivity(),"Not A Valid UK Passcode",Style.ALERT).show();
                return;
            }
        }catch (Exception e){}
    }

    public class AsyncUpdateUser extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL + Constants.Update_User_URL, GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            Crouton.makeText(getActivity(),"Profile Updated",Style.CONFIRM).show();
//            getActivity().getSupportFragmentManager().popBackStack();
//            PopulateModel(result);
        }
    }
    private void PopulateModel (String json) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json.toString());
            Gson gson = new Gson();
            RegisterUserModel obj ;
            obj = gson.fromJson(jsonObj.toString(),
                    RegisterUserModel.class);
            RegisterUserModel.getInstance().setList(obj);
            Log.e("status","/"+RegisterUserModel.getInstance().status+RegisterUserModel.getInstance().message);
            if(RegisterUserModel.getInstance().status == 200)
            {
                getActivity().finish();
                startActivity(new Intent(getActivity(), BaseActivity.class));
            }
        }catch (Exception e){}
    }
    public String  GetJsonObject() throws JSONException {

        Log.e("String",baseClass.getPhoneNumber()+"/"+baseClass.getExpROle()+"/"+baseClass.getExpTime()+"/"+baseClass.getTotalRoles()
                +"/"+baseClass.getEduLevel()+"/"+baseClass.getEligible()+"/"+ baseClass.getRadius()+"/"+baseClass.getLat()+","+baseClass.getLon());

        JSONObject object = new JSONObject();
        JSONObject userObj = new JSONObject();

        object.put("postcode", "SE1 1TL");
        object.put("city", "Karachi");
        object.put("location", baseClass.getLat()+","+baseClass.getLon());
        object.put("date_of_birth", "1983-06-12");
        object.put("first_name", baseClass.getFirstName());
        object.put("last_name", baseClass.getLastName());
        object.put("profile_image", "http,//www.matchninja.com/wp-content/uploads/2014/08/online-dating-profile.jpg");
        object.put("about_me", "I'm 23");
        object.put("experiences", baseClass.getExpTime());
        object.put("allowed_working_in_uk", baseClass.getEligible());
        object.put("search_job_radius", "100000000");
        object.put("is_available", true);
        object.put("is_active", false);
        object.put("education_level", baseClass.getEduLevel());

        userObj.put("job_seeker", object);
        userObj.put("interested_in",baseClass.getExpROle());
        userObj.put("experiences",baseClass.getTotalRoles());

        return userObj.toString();
    }
    private void PutValues() {
        baseClass.setFirstName(aq.id(R.id.first_name).getText().toString());
        baseClass.setLastName(aq.id(R.id.last_name).getText().toString());
        baseClass.setPostCode(aq.id(R.id.post_code).getText().toString());
        if(aq.id(R.id.school).isChecked())
            baseClass.setEduLevel("1");
        if(aq.id(R.id.college).isChecked())
            baseClass.setEduLevel("2");
        if(aq.id(R.id.university).isChecked())
            baseClass.setEduLevel("3");
//        if(aq.id(R.id.eligible).isChecked())
//            baseClass.setEligible(true);
//        else
//            baseClass.setEligible(false);
    }
    public void UpdateUI(Profile profile) {
        aq.id(R.id.facebook_button).background(R.drawable.facebook_button_green);
        baseClass.setFirstName(profile.getFirstName());
        baseClass.setLastName(profile.getLastName());
//        baseClass.setImageUrl(String.valueOf(profile.getProfilePictureUri(576, 576)));
        aq.id(R.id.first_name).text(profile.getFirstName());
        aq.id(R.id.last_name).text(profile.getLastName());
//        aq.id(R.id.profile_image).image(String.valueOf(profile.getProfilePictureUri(576, 576)));
    }

//    public void updateUILinkedin(Person profile){
//        aq.id(R.id.linked_in).background(R.drawable.linkedin_button_green);
//        baseClass.setFirstName(profile.getFirstName());
//        baseClass.setLastName(profile.getLastName());
//        baseClass.setImageUrl(String.valueOf(profile.getPictureUrl()));
//        aq.id(R.id.first_name).text(profile.getFirstName());
//        aq.id(R.id.last_name).text(profile.getLastName());
//        aq.id(R.id.profile_image).image(String.valueOf(profile.getPictureUrl()));
//    }

    private void logout(){
        aq.id(R.id.facebook_button).background(R.drawable.facebook_button_gray);
        baseClass.clearAll();
    }
    private boolean validated(){
        if (aq.id(R.id.first_name).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter first name", Style.ALERT).show();
            return false;
        }
        if (aq.id(R.id.last_name).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter last name", Style.ALERT).show();
            return false;
        }

        if (aq.id(R.id.post_code).getEditText().getText().toString().equals("")) {
            Crouton.makeText(getActivity(), "Please enter postcode", Style.ALERT).show();
            return false;
        }
        if (!aq.id(R.id.post_code).getEditText().getText().toString().contains(" ")) {
            Crouton.makeText(getActivity(), "Please enter correct UK postcode", Style.ALERT).show();
            return false;
        }
        if (!checkSelectionExists()) {
            Crouton.makeText(getActivity(), "Please choose at least one option from education level", Style.ALERT).show();
            return false;
        }
        return true;
    }
    private boolean checkSelectionExists() {
        return aq.id(R.id.school).isChecked() ||
                aq.id(R.id.college).isChecked() ||
                aq.id(R.id.university).isChecked();
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.DEFAULT);
        return imgString;
    }

}
