package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.algorepublic.yapjobs.DB.DaoMaster;
import com.algorepublic.yapjobs.DB.DaoSession;
import com.algorepublic.yapjobs.DB.RoleList;
import com.algorepublic.yapjobs.DB.Roles;
import com.algorepublic.yapjobs.DB.RolesDao;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import io.techery.progresshint.ProgressHintDelegate;

/**
 * Created by ahmad on 8/26/15.
 */
public class FragmentMyExperience extends BaseFragment {

    View view;
    AQuery aq;
    BaseClass baseClass;
    ArrayList<RoleList> lists;
    String val1,val2,val3;
    ProgressDialog dialog;

    public static FragmentMyExperience newInstance(){
        FragmentMyExperience fragmentMyExperience = new FragmentMyExperience();
        return fragmentMyExperience;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_experiences,container,false);
        aq = new AQuery(view);
        baseClass = ((BaseClass)getActivity().getApplicationContext());
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        lists = GetRolesFromDB();
        aq.id(R.id.my_experience).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Black));
        aq.id(R.id.experience_one).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.experience_two).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.experience_three).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Regular));
        aq.id(R.id.close_dialog).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lists.size() == 1) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1);
                }
                else if(lists.size() == 2) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1+" "+val2);
                }
                else if(lists.size() == 3) {
                    baseClass.setExpTime("");
                    baseClass.setExpTime(val1+" "+val2+" "+val3);
                }
                dialog.show();
                new AsyncUpdateUser().execute();

            }
        });

        if(lists.size()==1)
            ShowSeekBar1();
        if(lists.size()==2) {
            ShowSeekBar1();
            ShowSeekBar2();
        }
        if(lists.size()==3)
        {
            ShowSeekBar1();
            ShowSeekBar2();
            ShowSeekBar3();
        }

        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical1)).getHintDelegate()
                .setHintAdapter(new ProgressHintDelegate.SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val1 = String.valueOf(progress);
                        baseClass.setExpTime1(progress);
                        return String.valueOf(progress);
                    }
                });
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical2)).getHintDelegate()
                .setHintAdapter(new ProgressHintDelegate.SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val2 = String.valueOf(progress);
                        baseClass.setExpTime2(progress);
                        return String.valueOf(progress);
                    }
                });
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical3)).getHintDelegate()
                .setHintAdapter(new ProgressHintDelegate.SeekBarHintAdapter() {
                    @Override
                    public String getHint(android.widget.SeekBar seekBar, int progress) {
                        val3 = String.valueOf(progress);
                        baseClass.setExpTime3(progress);
                        return String.valueOf(progress);
                    }
                });
        return view;
    }

    private void ShowSeekBar1() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical1)).
                setProgress(baseClass.getExpTime1());
        view.findViewById(R.id.layout_experience1).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical1).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_one).text("Experince as " + lists.get(0).getRoleName());
    }
    private void ShowSeekBar2() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical2)).
                setProgress(baseClass.getExpTime2());
        view.findViewById(R.id.layout_experience2).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical2).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_two).text("Experince as " + lists.get(1).getRoleName());
    }
    private void ShowSeekBar3() {
        ((io.techery.progresshint.addition.widget.SeekBar) view.findViewById(R.id.seekbar_vertical3)).
                setProgress(baseClass.getExpTime3());
        view.findViewById(R.id.layout_experience3).setVisibility(View.VISIBLE);
        view.findViewById(R.id.seekbar_vertical3).setVisibility(View.VISIBLE);
        aq.id(R.id.experience_three).text("Experince as " + lists.get(2).getRoleName());
    }

    private ArrayList<RoleList> GetRolesFromDB( ) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                getActivity(), "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();
        RolesDao rolesDao = daoSession.getRolesDao();
        List<Roles> temp = rolesDao.queryBuilder().list();
        StringBuilder builder = new StringBuilder();
        for(int loop=0;loop<temp.size();loop++)
            builder.append(temp.get(loop).getRole_slug() + " ");
        baseClass.setExpROle(builder.toString().trim());
        ArrayList<RoleList> roleLists = new ArrayList<RoleList>();
        if(temp.size() !=0)
            for(int loop=0;loop<temp.size();loop++) {
                RoleList roleList = new RoleList();
                roleList.setRoleName(temp.get(loop).getRole_name());
                roleLists.add(roleList);
            }

        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
        return roleLists;
    }

    public class AsyncUpdateUser extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Update_User_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            Crouton.makeText(getActivity(), "Profile Updated", Style.CONFIRM).show();
//            getActivity().getSupportFragmentManager().popBackStack();
//            PopulateModel(result);
        }
    }

    public String  GetJsonObject() throws JSONException {

        Log.e("String", baseClass.getPhoneNumber() + "/" + baseClass.getExpROle() + "/" + baseClass.getExpTime() + "/" + baseClass.getTotalRoles()
                + "/" + baseClass.getEduLevel() + "/" + baseClass.getEligible() + "/" + baseClass.getRadius() + "/" + baseClass.getLat() + "," + baseClass.getLon());

        JSONObject object = new JSONObject();
        JSONObject userObj = new JSONObject();

        object.put("postcode", "SE1 1TL");
        object.put("city", "Karachi");
        object.put("location", baseClass.getLat()+","+baseClass.getLon());
        object.put("date_of_birth", "1983-06-12");
        object.put("first_name", baseClass.getFirstName());
        object.put("last_name", baseClass.getLastName());
        object.put("profile_image", "http,//www.matchninja.com/wp-content/uploads/2014/08/online-dating-profile.jpg");
        object.put("about_me", "I'm 23");
        object.put("experiences", baseClass.getExpTime());
        object.put("allowed_working_in_uk", baseClass.getEligible());
        object.put("search_job_radius", "100000000");
        object.put("is_available", true);
        object.put("is_active", false);
        object.put("education_level", baseClass.getEduLevel());

        userObj.put("job_seeker", object);
        userObj.put("interested_in", baseClass.getExpROle());
        userObj.put("experiences", baseClass.getTotalRoles());

        return userObj.toString();
    }
}
