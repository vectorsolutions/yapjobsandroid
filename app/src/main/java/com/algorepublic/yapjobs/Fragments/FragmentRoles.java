package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.algorepublic.yapjobs.Utils.TypeFaces;
import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import Adapters.AdapterJobRoles;
import Adapters.JobsRolesItems;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by ahmad on 8/26/15.
 */
public class FragmentRoles extends BaseFragment {

    View view;
    AQuery aq;
    BaseClass baseClass;
    ProgressDialog dialog;
    GridView gridView;

    public static FragmentRoles newInstance(){
        FragmentRoles fragmentRoles = new FragmentRoles();
        return fragmentRoles;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_roles,container,false);
        aq = new AQuery(view);
        baseClass = (BaseClass) getActivity().getApplicationContext();
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        gridView = (GridView) view.findViewById(R.id.gridView_role);
        aq.id(R.id.title).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Blod));
        aq.id(R.id.text1).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Light));
        aq.id(R.id.text2).getTextView().setTypeface(TypeFaces.get(getActivity(), BaseClass.SourceSansPro_Light));
        aq.id(R.id.close_dialog).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        dialog.show();

        aq.id(R.id.save).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                new AsyncUpdateUser().execute();
            }
        });

        new AsyncTry().execute();

        return view;
    }
    public class AsyncTry extends AsyncTask<Void, Void, String> {

        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {

            httpClient = new GenericHttpClient();
            try {
                response = httpClient.get(Constants.BASE_URL + Constants.JobsRoles_URL);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            PopulateModel(result);
        }
    }
    private void PopulateModel (String json) {
        JSONArray jsonArray;
        ArrayList<JobsRolesItems> arrayList = new ArrayList<JobsRolesItems>();
        try {
            jsonArray = new JSONArray(json);
            for (int loop = 0; loop <= jsonArray.length(); loop++) {
                JobsRolesItems jobsItems = new JobsRolesItems();
                jobsItems.setJobId(String.valueOf(jsonArray.getJSONObject(loop).getInt("id")));
                jobsItems.setName(String.valueOf(jsonArray.getJSONObject(loop).getString("name")));
                jobsItems.setImagUnselected(String.valueOf(jsonArray.getJSONObject(loop).getString("image")));
                jobsItems.setImagSelected(SplitRole(String.valueOf(jsonArray.getJSONObject(loop).getString("image"))) + "_unselected.png");
                jobsItems.setSlug(String.valueOf(jsonArray.getJSONObject(loop).getString("slug")));
                arrayList.add(jobsItems);
            }
        } catch (Exception e) {
        }
        AdapterJobRoles adapterJobRoles = new AdapterJobRoles(getActivity(), arrayList);
        gridView.setAdapter(adapterJobRoles);

    }

    public String SplitRole(String role)
    {
        String[] title = role.split("\\.");
        return title[0];
    }

    public class AsyncUpdateUser extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            try {

                httpClient = new GenericHttpClient();
                response = httpClient.post(Constants.BASE_URL+Constants.Update_User_URL ,GetJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            dialog.dismiss();
            Crouton.makeText(getActivity(), "Profile Updated", Style.CONFIRM).show();
//            getActivity().getSupportFragmentManager().popBackStack();
//            PopulateModel(result);
        }
    }

    public String  GetJsonObject() throws JSONException {

        Log.e("String", baseClass.getPhoneNumber() + "/" + baseClass.getExpROle() + "/" + baseClass.getExpTime() + "/" + baseClass.getTotalRoles()
                + "/" + baseClass.getEduLevel() + "/" + baseClass.getEligible() + "/" + baseClass.getRadius() + "/" + baseClass.getLat() + "," + baseClass.getLon());

        JSONObject object = new JSONObject();
        JSONObject userObj = new JSONObject();

        object.put("postcode", "SE1 1TL");
        object.put("city", "Karachi");
        object.put("location", baseClass.getLat()+","+baseClass.getLon());
        object.put("date_of_birth", "1983-06-12");
        object.put("first_name", baseClass.getFirstName());
        object.put("last_name", baseClass.getLastName());
        object.put("profile_image", "http,//www.matchninja.com/wp-content/uploads/2014/08/online-dating-profile.jpg");
        object.put("about_me", "I'm 23");
        object.put("experiences", baseClass.getExpTime());
        object.put("allowed_working_in_uk", baseClass.getEligible());
        object.put("search_job_radius", "100000000");
        object.put("is_available", true);
        object.put("is_active", false);
        object.put("education_level", baseClass.getEduLevel());

        userObj.put("job_seeker", object);
        userObj.put("interested_in",baseClass.getExpROle());
        userObj.put("experiences",baseClass.getTotalRoles());

        return userObj.toString();
    }

}
