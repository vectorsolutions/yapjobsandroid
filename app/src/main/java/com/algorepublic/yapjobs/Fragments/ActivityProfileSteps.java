package com.algorepublic.yapjobs.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.androidquery.AQuery;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;

import com.algorepublic.yapjobs.Fragments.FragmentVerifyDevice;

/**
 * Created by android on 8/18/15.
 */
public class ActivityProfileSteps extends AppCompatActivity {


    AQuery aq;
    BaseClass baseClass;
    public static CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_steps);
        aq = new AQuery(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        baseClass = ((BaseClass) getApplicationContext());
        if (savedInstanceState == null) {
            callFragment(R.id.container, FragmentVerifyDevice.newInstance(),"VerifyDevice");
        }

    }
    protected void callFragment(int containerId, Fragment fragment, String tag){
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerId,fragment,tag)
                .commit();
    }

}
