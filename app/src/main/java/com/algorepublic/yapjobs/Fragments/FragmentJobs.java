package com.algorepublic.yapjobs.Fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ListView;

import com.algorepublic.yapjobs.Adapters.JobsItems;
import com.algorepublic.yapjobs.Adapters.ListViewAdapter;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.algorepublic.yapjobs.Utils.GenericHttpClient;
import com.androidquery.AQuery;
import com.markupartist.android.widget.PullToRefreshListView;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by android on 8/12/15.
 */
public class FragmentJobs extends BaseFragment {


    AQuery aq;
    BaseClass baseClass;
    ProgressDialog dialog;
    ArrayList<JobsItems> arrayList;
    ListView listView;
    View view;
    int index;
    public  static ArrayList<JobsItems> resultitems;


    public static FragmentJobs newInstance() {
        FragmentJobs fragment = new FragmentJobs();
        return fragment;
    }

    public FragmentJobs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_jobs,container,false);
        aq =new AQuery(view);
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
      //  dialog.show();
        baseClass = ((BaseClass)getActivity().getApplicationContext());
        listView = (ListView) view.findViewById(R.id.listview);
        index=0;
        new AsyncTry().execute();
        ((PullToRefreshListView) aq.id(R.id.listview).getListView()).setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AsyncTry().execute();
            }
        });
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f,Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setFillAfter(true);
        anim.setRepeatMode(1);
        anim.setDuration(3000);
        aq.id(R.id.radar_pointer).getImageView().startAnimation(anim);
        return view;
    }

    public class AsyncTry extends AsyncTask<Void, Void, String> {
        GenericHttpClient httpClient;
        String response= null;
        @Override
        protected String doInBackground(Void... voids) {
            index++;
                httpClient = new GenericHttpClient();
            try {
                String phoneno = baseClass.getPhoneNumber().replace("+","%2B");
                String url = Constants.BASE_URL + Constants.Jobs_URL + "phone=" + phoneno + "&page="+index+"&size=1000000000";
                Log.e("url",url);
                response = httpClient.get(Constants.BASE_URL + Constants.Jobs_URL + "phone=" + phoneno + "&page="+index+"&size=10000000");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }
        @Override
        protected void onPostExecute(String result){
            ((PullToRefreshListView) aq.id(R.id.listview).getListView()).onRefreshComplete();
            dialog.dismiss();
            PopulateModel(result);
        }
    }
    private void PopulateModel (String json) {
        JSONArray jsonArray;
        arrayList =  new ArrayList<JobsItems>();
        try {
            jsonArray = new JSONArray(json);
            for(int loop=0;loop < jsonArray.length();loop++) {
                JobsItems jobsItems = new JobsItems();
                jobsItems.setJobId(String.valueOf(jsonArray.getJSONObject(loop).getInt("id")));
                jobsItems.setDistance(String.valueOf(jsonArray.getJSONObject(loop).getInt("distance")));
                jobsItems.setHourlyRate(String.valueOf(jsonArray.getJSONObject(loop).getDouble("hourly_rate")));
                jobsItems.setStartDate(String.valueOf(jsonArray.getJSONObject(loop).getString("start_date_format")));
                jobsItems.setBusinessName(String.valueOf(jsonArray.getJSONObject(loop).getJSONObject("business").getString("name")));
                jobsItems.setRoleName(String.valueOf(jsonArray.getJSONObject(loop).getJSONObject("role").getString("name")));
                jobsItems.setRoleImage(String.valueOf(jsonArray.getJSONObject(loop).getJSONObject("role").getString("image")));
                jobsItems.setLocation(String.valueOf(jsonArray.getJSONObject(loop).getString("location")));
                jobsItems.setAboutJob(String.valueOf(jsonArray.getJSONObject(loop).getString("about_job")));
//                jobsItems.setResponsibility(String.valueOf(jsonArray.getJSONObject(loop).getJSONObject("responsibilities").getString("name")));
                arrayList.add(jobsItems);
            }
        }catch (Exception e){}

      try {
          arrayList.addAll(FragmentJobs.resultitems);
      }catch (NullPointerException e){}

        aq.id(R.id.listview).adapter(new ListViewAdapter(getActivity(),arrayList));
    }
}