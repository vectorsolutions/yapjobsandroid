package Adapters;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.algorepublic.yapjobs.DB.DaoMaster;
import com.algorepublic.yapjobs.DB.DaoSession;
import com.algorepublic.yapjobs.DB.Roles;
import com.algorepublic.yapjobs.DB.RolesDao;
import com.algorepublic.yapjobs.R;
import com.algorepublic.yapjobs.Utils.BaseClass;
import com.algorepublic.yapjobs.Utils.Constants;
import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by android on 8/24/15.
 */
public class AdapterJobRoles extends BaseAdapter{

    private static ArrayList<JobsRolesItems> itemDetailarrayList;
    Context ctx;
    BaseClass baseClass;
    Boolean flag =false;
    public AdapterJobRoles(Context context, ArrayList<JobsRolesItems> results) {
        baseClass = ((BaseClass) context.getApplicationContext());
        itemDetailarrayList = results;
        this.ctx = context;
    }

    public int getCount() {
        return itemDetailarrayList.size();
    }

    public Object getItem(int position) {
        return itemDetailarrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
            convertView = inflater.inflate(R.layout.layout_jobroles_row, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.textview);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final AQuery aq = new AQuery(convertView);
        holder.title.setText(itemDetailarrayList.get(position).getName());
        holder.title.setTextColor(ctx.getResources().getColor(R.color.primary_green));
        aq.id(R.id.imageview).image(Constants.BASE_DEV + itemDetailarrayList.get(position).getImagUnselected());
        List<Roles> roles = GetRolesSizeFromDB();
        if (roles.size() > 0) {
            for (int loop = 0; loop < roles.size(); loop++) {
                if(Integer.parseInt(roles.get(loop).getRole_position()) ==  position)
                {
                    aq.id(R.id.checkbox).checked(true);
                    holder.title.setTextColor(ctx.getResources().getColor(R.color.white));
                    aq.id(R.id.imageview).image(Constants.BASE_DEV + itemDetailarrayList.get(position).getImagSelected());
                }
            }
        }

        aq.id(R.id.checkbox).getCheckBox().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aq.id(R.id.checkbox).isChecked()) {
                    GetRolesFromDB(String.valueOf(position), itemDetailarrayList.get(position).getName(), itemDetailarrayList.get(position).getSlug());
                    if (flag) {
                        holder.title.setTextColor(ctx.getResources().getColor(R.color.white));
                        aq.id(R.id.imageview).image(Constants.BASE_DEV + itemDetailarrayList.get(position).getImagSelected());
                    } else
                        aq.id(R.id.checkbox).checked(false);
                } else {
                    DeleteRolesFromDB(String.valueOf(position));
                    holder.title.setTextColor(ctx.getResources().getColor(R.color.primary_green));
                    aq.id(R.id.imageview).image(Constants.BASE_DEV + itemDetailarrayList.get(position).getImagUnselected());
                }
            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView title;
        CheckBox checkBox;
    }

    private void GetRolesFromDB( String position,String name,String roleSlug) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                ctx, "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();

        RolesDao rolesDao = daoSession.getRolesDao();
        List<Roles> temp = rolesDao.queryBuilder().list();

        if (temp.size() < 3) {

            try {
                baseClass.setTotalRoles(temp.size() + 1);
                AddedRolesToDB(position, name,roleSlug);
            } catch (Exception e) {
            }
            flag =true;
        } else {
            flag =false;
            baseClass.setTotalRoles(temp.size());
            Crouton.makeText((Activity) ctx, "You can select maximum of 3 roles", Style.ALERT).show();
        }

        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
    }
    private List<Roles> GetRolesSizeFromDB( ) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                ctx, "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();

        RolesDao rolesDao = daoSession.getRolesDao();
        List<Roles> temp = rolesDao.queryBuilder().list();
        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
        return temp;
    }
    public void AddedRolesToDB(String rolePosition, String roleName,String roleSlug) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                ctx, "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj
                .getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();

        RolesDao bookListDao = daoSession.getRolesDao();
        Roles bookList = new Roles(rolePosition, roleName,roleSlug);
        bookListDao.insert(bookList);
        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
    }
    public void DeleteRolesFromDB(String rolePosition) {
        DaoMaster.DevOpenHelper ex_database_helper_obj = new DaoMaster.DevOpenHelper(
                ctx, "yapjobs.sqlite", null);
        SQLiteDatabase ex_db = ex_database_helper_obj
                .getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(ex_db);
        DaoSession daoSession = daoMaster.newSession();

        RolesDao bookListDao = daoSession.getRolesDao();
        bookListDao.deleteByKey(rolePosition);
        daoSession.clear();
        ex_db.close();
        ex_database_helper_obj.close();
    }
}
