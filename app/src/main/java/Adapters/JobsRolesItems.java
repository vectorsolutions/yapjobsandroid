package Adapters;

/**
 * Created by android on 8/24/15.
 */
public class JobsRolesItems {

    public void  setJobId(String id)
    {
        this.jobId = id;
    }
    public String  getJobId()
    {
        return jobId ;
    }
    public void  setName(String name)
    {
        this.name = name;
    }
    public String  getName()
    {
        return name ;
    }
    public void  setImagSelected(String image)
    {
        this.imagSelected = image;
    }
    public String  getImagSelected()
    {
        return imagSelected ;
    }
    public void  setImagUnselected(String image)
    {
        this.imagUnselected = image;
    }
    public String  getImagUnselected()
    {
        return imagUnselected ;
    }
    public void  setSlug(String slug)
    {
        this.slug = slug;
    }
    public String  getSlug()
    {
        return slug ;
    }


    private String jobId ;
    private String name ;
    private String imagSelected;
    private String imagUnselected;
    private String slug;
}
